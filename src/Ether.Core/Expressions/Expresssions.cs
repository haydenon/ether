using System.Collections.Generic;
using System.Linq;
using Ether.Core.Statements;
using Ether.Core.Tokens;
using Ether.Core.Types;

namespace Ether.Core.Expressions
{
    public interface IExpr
    {
        IToken RepresentingToken { get; }
    }

    public record Binary(IExpr Left, IToken Op, IExpr Right) : IExpr
    {
        public IToken RepresentingToken => Op;

        public override string ToString()
            => $"{Left.ToString()} {Op.Lexeme} {Right.ToString()}";
    }

    public record Unary(IToken Op, IExpr Right) : IExpr
    {
        public IToken RepresentingToken => Op;
        public override string ToString()
            => $"{Op.Lexeme} {Right.ToString()}";
    }

    public record Literal(IToken Token, object Data, IType Type) : IExpr
    {
        public IToken RepresentingToken => Token;
        public override string ToString()
            => (Type == Primatives.StringType ? '"' + Data.ToString() + '"' : Data.ToString()) ?? "";
    }

    public record ArrayConstructor(IToken Token, IList<IExpr> Data, IType? TypeAnnotation) : IExpr
    {
        public IToken RepresentingToken => Token;
        public override string ToString()
            => $"[{string.Join(", ", Data.Select(d => d.ToString()))}]";
    }

    public record Logical(IExpr Left, IToken Op, IExpr Right) : IExpr
    {
        public IToken RepresentingToken => Op;
        public override string ToString()
            => $"{Left.ToString()} {Op.Lexeme} {Right.ToString()}";
    }

    public record Grouping(IExpr Expr) : IExpr
    {
        public IToken RepresentingToken => Expr.RepresentingToken;
        public override string ToString()
            => $"({Expr.ToString()})";
    }

    public record IfExpr(IExpr Condition, IExpr IfTrue, IExpr? IfFalse) : IExpr
    {
        public IToken RepresentingToken => Condition.RepresentingToken;
        private string ElseCondition => IfFalse != null ? $" else {IfFalse.ToString()}" : string.Empty;
        public override string ToString() => $"if {Condition.ToString()} then {IfTrue.ToString()}{ElseCondition}";
    }

    public record Variable(IToken Name) : IExpr
    {
        public IToken RepresentingToken => Name;
        public override string ToString() => $"{Name.Lexeme}";
    }

    public record CallExpr(IExpr Callee, IToken Paren, IList<IExpr> Arguments) : IExpr
    {
        public IToken RepresentingToken => Paren;
        public override string ToString() => $"{Callee.ToString()}({string.Join(", ", Arguments.Select(a => a.ToString()))})";
    }
    public record ReturnExpr(IToken ReturnToken, IExpr ReturnValue) : IExpr
    {
        public IToken RepresentingToken => ReturnToken;

        public override string ToString() => $"return {ReturnValue}";
    }

    public record Block(List<IStmt> Statements) : IExpr
    {
        public IToken RepresentingToken => Statements.First().RepresentingToken;
        public override string ToString()
            => $"{{\n{string.Join("\n", Statements.Select(s => s.ToString()))}\n}}";
    }

    public record FunctionParameter(IToken Name, IType? TypeAnnotation)
    {
        private string TypeAnnotationString =>
            TypeAnnotation != null ? $": {TypeAnnotation.ToString()}" : string.Empty;
        public override string ToString() => $"{Name.Lexeme}{TypeAnnotationString}";

        public static FunctionParameter UnitParameter(int line) => new FunctionParameter(new EmptyToken(TokenType.Identifier, "$", line), Primatives.UnitType);
    }

    public record Function(IList<FunctionParameter> Parameters, IExpr Body, IType? ReturnType) : IExpr
    {
        public IToken RepresentingToken => Body.RepresentingToken;
        private string ReturnAnnotation => ReturnType != null ? $": {ReturnType.ToString()}" : string.Empty;
        public override string ToString() => $"({string.Join(", ", Parameters.Select(p => p.ToString()))}){ReturnAnnotation} => {Body.ToString()}";
    }

    public record Get(IExpr Obj, IToken Name) : IExpr
    {
        public IToken RepresentingToken => Name;
        public override string ToString() => $"{Obj.ToString()}.{Name.Lexeme}";
    }

    public record Set(IExpr Obj, IToken Name) : IExpr
    {
        public IToken RepresentingToken => Name;
        public override string ToString() => $"{Obj.ToString()}.{Name.Lexeme} = ...";
    }

    public record This(IToken Keyword) : IExpr
    {
        public IToken RepresentingToken => Keyword;
        public override string ToString() => $"this";
    }
}