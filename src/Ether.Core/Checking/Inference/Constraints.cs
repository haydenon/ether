using Ether.Core.Tokens;
using Ether.Core.Types;

namespace Ether.Core.Checking.Inference
{
    public interface IConstraint
    {
        public IToken Location { get; }
    }

    public record CEquality(IType T1, IType T2, IToken Location) : IConstraint
    {
        public override string ToString() => $"EqualityConstraint:<{T1.ToString()}> == <{T2.ToString()}>";
    }
}