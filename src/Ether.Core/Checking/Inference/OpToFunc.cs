using Ether.Core.Expressions;
using Ether.Core.Tokens;

namespace Ether.Core.Checking.Inference
{
    public static class OpToFunc
    {
        public static CallExpr BinToCall(Binary bin)
            => new CallExpr(new Variable(CloneOp(bin.Op, true)), bin.Op, new[] { bin.Left, bin.Right });

        public static CallExpr UnToFunc(Unary un)
            => new CallExpr(new Variable(CloneOp(un.Op, false)), un.Op, new[] { un.Right });

        private static IToken CloneOp(IToken op, bool bin)
            => new EmptyToken(op.TokenType, $"${op.Lexeme}_{(bin ? 2 : 1)}", op.Line);
    }
}