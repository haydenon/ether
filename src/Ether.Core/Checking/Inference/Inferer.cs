using System.Collections.Generic;
using System.Linq;
using Ether.Core.Expressions;
using Ether.Core.Types;
using Ether.Core.Tokens;
using static Ether.Core.Common;
using static Ether.Core.Checking.Inference.OpToFunc;
using static Ether.Core.Checking.Inference.Messages;
using Ether.Core.Statements;
using System;

namespace Ether.Core.Checking.Inference
{
    public class Inferer
    {
        private readonly List<IType> substitution = new List<IType>();
        private readonly List<IConstraint> typeConstraints = new List<IConstraint>();

        private Inferer() { }

        private IType? CurrentReturnType;

        public static Result<Unit> InferProgram(Program program)
        {
            var inferer = new Inferer();
            var defaultContext = DefaultContext.Value;

            try
            {
                var (newStatements, updatedCtx) = inferer.InferStatements(program.Statements, defaultContext);
                inferer.SolveConstraints();
                var substitutedCtx = updatedCtx.MapTypes(inferer.Substitute);
                inferer.EnsureResolved(newStatements, substitutedCtx);
                return Ok(unit);
            }
            catch (InferenceException ex)
            {
                return Err<Unit>(new InferenceCheckError(ex.Message, ex.Location));
            }
        }

        public static IType Infer(IExpr expr, IType? expectedType, Context ctx)
        {
            var inferer = new Inferer();
            var newExpr = inferer.InferType(
                expr,
                expectedType ?? inferer.NewTypeVariable(expr.RepresentingToken),
                ctx
            );
            inferer.SolveConstraints();
            return inferer.SubstituteExpression(newExpr, ctx).type;
        }

        private void EnsureResolved(List<IStmt> stmts, Context ctx)
        {
            foreach (var statement in stmts)
            {
                RunForStmtExpr(statement, ctx, EnsureNotTypeVariable);
            }
        }

        private T RunForStmtExpr<T>(IStmt stmt, Context ctx, Func<IExpr, Context, T> action)
        {
            switch (stmt)
            {
                case LetStmt { Value: IExpr expr }:
                    return action(expr, ctx);
                case PrintStmt { Value: IExpr expr }:
                    return action(expr, ctx);
                case ExprStmt { Value: IExpr expr }:
                    return action(expr, ctx);
                case ReturnStmt { Value: IExpr expr }:
                    return action(expr, ctx);
                default:
                    throw new InferenceException($"Statement {stmt} is not supported for inference", stmt.RepresentingToken);
            }
        }

        private Unit EnsureNotTypeVariable(IExpr expr, Context ctx)
        {
            if (ContainsTypeVariable(SubstituteExpression(expr, ctx).type, expr.RepresentingToken))
            {
                throw new InferenceException("Cannot infer type for expression", expr.RepresentingToken);
            }

            return unit;
        }

        private bool ContainsTypeVariable(IType type, IToken token)
            => type switch
            {
                TPlain => false,
                ITConstructor { Generics: IList<IType> generics } => generics.Any(t => ContainsTypeVariable(t, token)),
                TVariable => true,
                _ => throw new InferenceException($"Cannot check whether type {type.ToString()} contains a variable", token)
            };

        private (List<IStmt>, Context) InferStatements(List<IStmt> stmts, Context ctx)
            => stmts.Aggregate((new List<IStmt>(), ctx), (res, stmt) =>
            {
                var (stmts, upd) = res;
                var (newStmt, updated) = InferStatementImpl(stmt, upd);
                stmts.Add(newStmt);
                return (stmts, updated);
            });

        private (IStmt, Context) InferStatementImpl(IStmt stmt, Context ctx)
            => stmt switch
            {
                LetStmt let => InferLetStmt(let, ctx),
                PrintStmt print => InferPrintStmt(print, ctx),
                ExprStmt expr => InferExprStmt(expr, ctx),
                _ => throw new InferenceException($"Statement {stmt} is not supported for inference", stmt.RepresentingToken)
            };

        private (IStmt, Context) InferExprStmt(ExprStmt expr, Context ctx)
        {
            var expectedType = NewTypeVariable(expr.Value.RepresentingToken);
            var newExpr = InferType(expr.Value, expectedType, ctx);
            return (new ExprStmt(newExpr), ctx);
        }

        private (IStmt, Context) InferLetStmt(LetStmt let, Context ctx)
        {
            var expectedType = let.ExpectedType ?? NewTypeVariable(let.Value.RepresentingToken);
            var expr = InferType(let.Value, expectedType, ctx);
            var type = SubstituteExpression(expr, ctx).type;
            return (new LetStmt(let.Name, expr, let.ExpectedType), ctx.Add(type, let.Name.Lexeme));
        }

        private (IStmt, Context) InferPrintStmt(PrintStmt print, Context ctx)
        {
            var expr = InferType(print.Value, NewTypeVariable(print.Value.RepresentingToken), ctx);
            return (new PrintStmt(expr), ctx);
        }

        // public static Result<Context> InferStatements(List<IStmt> stmts, Context ctx)
        //     => stmts.Aggregate(Ok(ctx), (res, stmt) => res.Bind(upd => InferStatement(stmt, upd)));

        // public static Result<Context> InferStatement(IStmt stmt, Context ctx)
        // {
        //     try
        //     {
        //         return Ok(InferStatementImpl(stmt, ctx));
        //     }
        //     catch (InferenceException ex)
        //     {
        //         return Err<Context>(new InferenceCheckError(ex.Message, ex.Location));
        //     }
        // }

        private IExpr InferType(IExpr expr, IType expectedType, Context ctx)
            => expr switch
            {
                Literal l => InferLiteral(l, expectedType, ctx),
                Function func => InferFunc(func, expectedType, ctx),
                CallExpr call => InferCall(call, expectedType, ctx),
                Variable v => InferVariable(v, expectedType, ctx),
                Binary bin => InferCall(BinToCall(bin), expectedType, ctx),
                Unary un => InferCall(UnToFunc(un), expectedType, ctx),
                Grouping grp => InferType(grp.Expr, expectedType, ctx),
                ArrayConstructor arr => InferArray(arr, expectedType, ctx),
                IfExpr ifExpr => InferIf(ifExpr, expectedType, ctx),
                Block block => InferBlock(block, expectedType, ctx),
                ReturnExpr ret => InferReturn(ret, expectedType, ctx),
                _ => throw new InferenceException("Unhandled inference of expressions", expr.RepresentingToken)
            };

        private Unit SolveConstraints()
            => typeConstraints.Aggregate(unit, SolveConstraint);

        private Unit SolveConstraint(Unit acc, IConstraint cons)
            => cons switch
            {
                CEquality { T1: IType t1, T2: IType t2, Location: IToken loc } => Unify(t1, t2, loc),
                _ => unit
            };

        private IStmt With(IStmt stmt, Context ctx, Func<IType, IExpr, IStmt> replace)
            => RunForStmtExpr(stmt, ctx, (expr, upd) =>
            {
                var (type, newExpr) = SubstituteExpression(expr, upd);
                return replace(type, newExpr);
            });

        private IStmt SubstituteStatement(IStmt stmt, Context ctx)
            => stmt switch
            {
                LetStmt let => With(let, ctx, (type, expr) => new LetStmt(let.Name, expr, type)),
                PrintStmt print => With(print, ctx, (type, expr) => new PrintStmt(expr)),
                ExprStmt expr => With(expr, ctx, (type, expr) => new ExprStmt(expr)),
                _ => throw new InferenceException("Unhandled substitution of statements", stmt.RepresentingToken)
            };

        private (IType type, IExpr expr) SubstituteExpression(IExpr expr, Context ctx)
        {
            switch (expr)
            {
                case Function { Parameters: IList<FunctionParameter> p, ReturnType: IType retType, Body: IExpr body }:
                    {
                        var newReturnType = Substitute(retType);
                        var newParams = p.Select(fp => fp with { TypeAnnotation = Substitute(fp.TypeAnnotation!) }).ToArray();
                        var bodyCtx = ctx.AddRange(newParams.Select(p => (p.TypeAnnotation!, p.Name.Lexeme)));
                        var (bodyType, newBody) = SubstituteExpression(body, bodyCtx);
                        var funcTypes = newParams.Select(p => p.TypeAnnotation!).Concat(new[] { newReturnType }).ToArray();
                        return (new TFunc(funcTypes), new Function(newParams, newBody, newReturnType));
                    }
                case CallExpr { Callee: IExpr callee, Arguments: IList<IExpr> args }:
                    {
                        var (funcType, newFunction) = SubstituteExpression(callee, ctx);
                        var newArguments = args.Select(a => SubstituteExpression(a, ctx).expr).ToArray();
                        var returnFuncType = funcType is TFunc func && func.Generics.Count > args.Count
                            ? ApplyCall(func, args.Count)
                            : (funcType as TFunc ?? throw new InferenceException("", expr.RepresentingToken)).Generics.Last();
                        return (returnFuncType, new CallExpr(newFunction, expr.RepresentingToken, newArguments));
                    }
                case Variable v:
                    return (ctx[v.Name.Lexeme]!, v);
                case Literal l:
                    return (l.Type, l);
                case ArrayConstructor { TypeAnnotation: IType itemType, Data: IList<IExpr> items }:
                    {
                        var newItemType = Substitute(itemType);
                        var newItems = items.Select(i => SubstituteExpression(i, ctx).expr).ToArray();
                        var newArr = new ArrayConstructor(expr.RepresentingToken, newItems, newItemType);
                        return (new TConstructor("Array", new[] { newItemType }), newArr);
                    }
                case IfExpr { Condition: IExpr cond, IfTrue: ReturnExpr ifTrue, IfFalse: ReturnExpr ifFalse }:
                    {
                        var (ifTrueRetType, newTrueExpr) = SubstituteExpression(ifTrue.ReturnValue, ctx);
                        var (ifFalseRetType, newFalseExpr) = SubstituteExpression(ifFalse.ReturnValue, ctx);

                        var (_, condExpr) = SubstituteExpression(cond, ctx);

                        return (ifTrueRetType, new IfExpr(
                            condExpr,
                            new ReturnExpr(ifTrue.RepresentingToken, newTrueExpr),
                            new ReturnExpr(ifFalse.RepresentingToken, newFalseExpr)
                        ));
                    }
                case IfExpr { Condition: IExpr cond, IfTrue: ReturnExpr ifTrue, IfFalse: IExpr ifFalse }:
                    {
                        var (ifTrueRetType, newTrueExpr) = SubstituteExpression(ifTrue.ReturnValue, ctx);
                        var (ifFalseType, newFalseExpr) = SubstituteExpression(ifFalse, ctx);

                        var (_, condExpr) = SubstituteExpression(cond, ctx);

                        return (ifFalseType, new IfExpr(
                            condExpr,
                            new ReturnExpr(ifTrue.RepresentingToken, newTrueExpr),
                            new ReturnExpr(ifFalse.RepresentingToken, newFalseExpr)
                        ));
                    }
                case IfExpr { Condition: IExpr cond, IfTrue: IExpr ifTrue, IfFalse: ReturnExpr ifFalse }:
                    {
                        var (ifTrueType, newTrueExpr) = SubstituteExpression(ifTrue, ctx);
                        var (ifFalseRetType, newFalseExpr) = SubstituteExpression(ifFalse.ReturnValue, ctx);

                        var (_, condExpr) = SubstituteExpression(cond, ctx);

                        return (ifTrueType, new IfExpr(
                            condExpr,
                            new ReturnExpr(ifTrue.RepresentingToken, newTrueExpr),
                            new ReturnExpr(ifFalse.RepresentingToken, newFalseExpr)
                        ));
                    }
                case IfExpr { Condition: IExpr cond, IfTrue: IExpr ifTrue, IfFalse: IExpr ifFalse }:
                    {
                        var (_, condExpr) = SubstituteExpression(cond, ctx);
                        var (ifType, ifTrueExpr) = SubstituteExpression(ifTrue, ctx);
                        var (_, ifFalseExpr) = SubstituteExpression(ifFalse, ctx);
                        return (ifType, new IfExpr(condExpr, ifTrueExpr, ifFalseExpr));
                    }
                case Block { Statements: List<IStmt> statements }:
                    {
                        if (statements.Last() is ReturnStmt { Value: IExpr exprReturn } retStmt)
                        {
                            var newStatements = statements.Take(statements.Count - 1)
                                .Select(stmt => SubstituteStatement(stmt, ctx)).ToList();
                            var (returnType, newExprReturn) = SubstituteExpression(exprReturn, ctx);
                            newStatements.Add(retStmt is ExprReturnStmt
                                ? new ExprReturnStmt(newExprReturn)
                                : new ReturnStmt(newExprReturn));
                            return (returnType, new Block(newStatements));
                        }
                        else
                        {
                            var newStatements = statements.Take(statements.Count - 1)
                                .Select(stmt => SubstituteStatement(stmt, ctx)).ToList();
                            return (Primatives.UnitType, new Block(newStatements));
                        }
                    }
                default:
                    throw new InferenceException("Unhandled substitution of expressions", expr.RepresentingToken);
            }
        }

        private static TFunc ApplyCall(TFunc func, int argCount)
        {
            var generics = func.Generics.Skip(argCount).ToArray();
            if (generics.Length == 1 && generics[0] is TFunc f)
            {
                return f;
            }

            return new TFunc(generics);
        }

        private IType Substitute(IType t)
            => t switch
            {
                TVariable v when SubstNotTypeVariable(v.index, v) => Substitute(substitution[v.index]),
                TConstructor { Name: string n, Generics: IList<IType> g } => new TConstructor(n, g.Select(Substitute).ToList()),
                TFunc { Generics: IList<IType> g } => new TFunc(g.Select(Substitute).ToList()),
                _ => t,
            };



        private IExpr InferLiteral(Literal l, IType expectedType, Context ctx)
        {
            typeConstraints.Add(new CEquality(expectedType, l.Type, l.RepresentingToken));
            return l;
        }

        private IExpr InferArray(ArrayConstructor arr, IType expectedType, Context ctx)
        {
            var newItemType = arr.TypeAnnotation ?? NewTypeVariable(arr.RepresentingToken);
            var newItems = arr.Data.Select(i => InferType(i, newItemType, ctx)).ToArray();
            typeConstraints.Add(new CEquality(expectedType, new TConstructor("Array", new[] { newItemType }), arr.RepresentingToken));
            return new ArrayConstructor(arr.Token, newItems, newItemType);
        }

        private IExpr InferVariable(Variable v, IType expectedType, Context ctx)
        {
            var type = ctx[v.Name.Lexeme] ?? throw new InferenceException(UnboundVariableError(v), v.RepresentingToken);
            // Curried function
            if (type is TFunc func &&
                expectedType is TFunc expectedFunc &&
                func.Generics.Count > expectedFunc.Generics.Count)
            {
                var curriedCount = expectedFunc.Generics.Count - 1;
                var curriedGenerics =
                    func.Generics.Take(curriedCount)
                        .Concat(new[] { new TFunc(func.Generics.Skip(curriedCount).ToArray()) })
                        .ToArray();
                var curriedFunc = new TFunc(curriedGenerics);
                typeConstraints.Add(new CEquality(expectedType, curriedFunc, v.RepresentingToken));
            }
            else
            {
                typeConstraints.Add(new CEquality(expectedType, type, v.RepresentingToken));
            }

            return v;
        }

        private IExpr InferReturn(ReturnExpr retExpr, IType expectedType, Context ctx)
        {
            if (CurrentReturnType == null)
            {
                throw new InferenceException(Messages.ReturnOutOfFunction, retExpr.RepresentingToken);
            }

            var newReturnValue = InferType(retExpr.ReturnValue, CurrentReturnType, ctx);
            return new ReturnExpr(retExpr.ReturnToken, newReturnValue);
        }

        private IExpr InferIf(IfExpr ifExpr, IType expectedType, Context ctx)
        {
            var ifType = ifExpr.IfFalse == null
                ? Primatives.UnitType
                : NewTypeVariable(ifExpr.RepresentingToken);

            var newCondition = InferType(ifExpr.Condition, Primatives.BoolType, ctx);
            var newIfTrue = InferType(ifExpr.IfTrue, ifType, ctx);
            var ifFalse = ifExpr.IfFalse != null
                ? ifExpr.IfFalse
                : new Literal(ifExpr.RepresentingToken, unit, Primatives.UnitType);
            var newIfFalse = InferType(ifFalse, ifType, ctx);

            typeConstraints.Add(new CEquality(expectedType, ifType, ifExpr.RepresentingToken));
            return new IfExpr(newCondition, newIfTrue, newIfFalse);
        }

        private IExpr InferBlock(Block block, IType expectedType, Context ctx)
        {
            var statements = block.Statements;
            if (statements.Last() is ReturnStmt { Value: IExpr returnExpr } retStmt)
            {
                var stmts = statements.Take(statements.Count - 1).ToList();
                var (newStatements, blockCtx) = InferStatements(stmts, ctx);
                if (blockCtx == null)
                {
                    throw new InferenceException("Invalid block", block.RepresentingToken);
                }
                var newReturn = InferType(returnExpr, expectedType, blockCtx);
                newStatements.Add(retStmt is ExprReturnStmt
                    ? new ExprReturnStmt(newReturn)
                    : new ReturnStmt(newReturn));
                return new Block(newStatements);
            }
            if (statements.Last() is ExprStmt { Value: IfExpr { Condition: IExpr cond, IfTrue: ReturnExpr ifTrue, IfFalse: ReturnExpr ifFalse } ifExpr })
            {
                var stmts = statements.Take(statements.Count - 1).ToList();
                var (newStatements, blockCtx) = InferStatements(stmts, ctx);
                if (blockCtx == null)
                {
                    throw new InferenceException("Invalid block", block.RepresentingToken);
                }

                var newCondition = InferType(cond, Primatives.BoolType, ctx);
                var newIfTrue = InferType(ifTrue, expectedType, blockCtx);
                var newIfFalse = InferType(ifFalse, expectedType, blockCtx);

                newStatements.Add(new ReturnStmt(new IfExpr(newCondition, newIfTrue, newIfFalse)));
                return new Block(newStatements);
            }
            else
            {
                typeConstraints.Add(new CEquality(expectedType, Primatives.UnitType, block.RepresentingToken));
                var (newStatements, _) = InferStatements(statements, ctx);
                return new Block(newStatements);
            }
        }

        private IExpr InferFunc(Function func, IType expectedType, Context ctx)
        {
            if (expectedType is TFunc { Generics: IList<IType> gen } expectedFunc
                && (gen.Count - 1) < func.Parameters.Count)
            {
                return InferCurriedFunc(func, expectedFunc, ctx);
            }

            var newReturnType = func.ReturnType ?? NewTypeVariable(func.RepresentingToken);

            return HandleFunctionInference(
                func.RepresentingToken,
                func.Parameters,
                func.Body,
                newReturnType,
                expectedType,
                ctx
            );
        }

        private IExpr InferCurriedFunc(Function func, TFunc expectedFunc, Context ctx)
        {
            // This needs extra testing
            var curried = func.Parameters.Take(expectedFunc.Generics.Count - 1).ToArray();
            var remaining = func.Parameters.Skip(expectedFunc.Generics.Count - 1).ToArray();

            var remainingParameterTypes = remaining.Select(p => p.TypeAnnotation ?? NewTypeVariable(p.Name)).ToArray();
            // Do we need to zip?
            var newReturnType = func.ReturnType ?? NewTypeVariable(func.RepresentingToken);
            var returnFuncGenerics = remainingParameterTypes.Concat(new[] { newReturnType }).ToArray();
            var returnFuncType = new TFunc(returnFuncGenerics);
            var newBodyFunc = new Function(remaining, func.Body, newReturnType);

            return HandleFunctionInference(
                func.RepresentingToken,
                curried,
                newBodyFunc,
                returnFuncType,
                expectedFunc,
                ctx
            );
        }

        private IExpr HandleFunctionInference(
            IToken location,
            IList<FunctionParameter> parameters,
            IExpr body,
            IType returnType,
            IType expectedType,
            Context ctx)
        {
            var previousType = CurrentReturnType;
            CurrentReturnType = returnType;

            var newParameterTypes = parameters.Select(p => p.TypeAnnotation ?? NewTypeVariable(p.Name)).ToArray();
            var newParameters = parameters.Zip(newParameterTypes)
                .Select(pt => pt.First with { TypeAnnotation = pt.Second }).ToArray();
            var newContext = ctx.AddRange(newParameters.Select(p => (p.TypeAnnotation!, p.Name.Lexeme)));
            var newBody = InferType(body, returnType, newContext);

            CurrentReturnType = previousType;

            var functionType = new TFunc(newParameterTypes.Concat(new[] { returnType }).ToArray());
            typeConstraints.Add(new CEquality(expectedType, functionType, location));
            return new Function(newParameters, newBody, returnType);
        }


        private IExpr InferCall(CallExpr call, IType expectedType, Context ctx)
        {
            var argumentTypes = call.Arguments.Select(a => NewTypeVariable(a.RepresentingToken)).ToArray();
            var functionType = new TFunc(argumentTypes.Concat(new[] { expectedType }).ToArray());
            var newFunction = InferType(call.Callee, functionType, ctx);
            var newArguments = call.Arguments.Zip(argumentTypes).Select(at => InferType(at.First, at.Second, ctx)).ToArray();
            return new CallExpr(newFunction, call.Paren, newArguments);
        }

        private Unit Unify(IType t1, IType t2, IToken loc)
            => (t1, t2) switch
            {
                (ITConstructor c1, ITConstructor c2) => HandleConstructors(c1, c2, loc),
                (TVariable v1, TVariable v2) when v1.index == v2.index => unit,
                (TVariable v, _) when SubstNotTypeVariable(v.index, v) => Unify(substitution[v.index], t2, loc),
                (_, TVariable v) when SubstNotTypeVariable(v.index, v) => Unify(t1, substitution[v.index], loc),
                (TVariable v, _) => HandleVar(v, t2, loc),
                (_, TVariable v) => HandleVar(v, t1, loc),
                _ => throw new InferenceException("Unhandled unification of types", loc)
            };

        private Unit HandleConstructors(ITConstructor c1, ITConstructor c2, IToken loc)
        {
            var (name1, gen1) = c1;
            var (name2, gen2) = c2;
            if (name1 != name2)
            {
                var message = (c1, c2) switch
                {
                    (TFunc f1, TFunc f2) when f2.Generics.Count <= 1 => NotAFunction,
                    (TFunc f1, TFunc f2) when f1.Generics.Count > f2.Generics.Count => TooManyArguments(f2.Generics.Count - 1, f1.Generics.Count - 1),
                    _ => TypeMismatchError(c1, c2)
                };
                throw new InferenceException(message, loc);
            }
            if (gen1.Count != gen2.Count)
            {
                throw new InferenceException($"{c1.ToString()} and {c2.ToString()} have a different number of generic arguments", loc);
            }

            foreach (var (t1, t2) in gen1.Zip(gen2))
            {
                Unify(t1, t2, loc);
            }
            return unit;
        }

        private Unit HandleVar(TVariable variable, IType other, IToken loc)
        {
            var i = variable.index;
            if (OccursIn(i, other, loc))
            {
                throw new InferenceException($"Type {variable.ToString()} occurs in other type {other.ToString()}", loc);
            }

            substitution[i] = other;
            return unit;
        }

        private bool OccursIn(int index, IType t, IToken loc)
            => t switch
            {
                TVariable v when SubstNotTypeVariable(v.index, t) => OccursIn(v.index, substitution[v.index], loc),
                TVariable v => v.index == index,
                ITConstructor(_, IList<IType> generics) => generics.Any(t => OccursIn(index, t, loc)),
                _ => throw new InferenceException("Unhandled unification of types", loc)
            };

        private bool SubstNotTypeVariable(int index, IType t2)
            => index >= substitution.Count ||
                !substitution[index].Equals(t2);

        private TVariable NewTypeVariable(IToken loc)
        {
            var res = new TVariable(substitution.Count);
            substitution.Add(res);
            return res;
        }
    }
}