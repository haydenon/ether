using System;
using Ether.Core.Tokens;

namespace Ether.Core.Checking.Inference
{
    public class InferenceException : Exception
    {
        public InferenceException(string message, IToken location) : base(message)
        {
            Location = location;
        }

        public IToken Location { get; }
    }
}