using Ether.Core.Expressions;
using Ether.Core.Types;

namespace Ether.Core.Checking.Inference
{
    public static class Messages
    {
        public static string TypeMismatchError(IType expected, IType found)
            => $"Type mismatch:\n  Expected: {expected.ToString()}\n     Found: {found.ToString()}";

        public static string UnboundVariableError(Variable v)
            => $"Unbound var {v.Name.Lexeme}";

        public static string TooManyArguments(int max, int provided)
            => $"Function call can only take at most {max} arguments, but {provided} arguments provided";

        public const string NotAFunction = "Value is not a function";
        public const string ReturnOutOfFunction = "Cannot use 'return' keyword outside of a function";
    }
}