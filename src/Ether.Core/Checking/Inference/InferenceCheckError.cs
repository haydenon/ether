using Ether.Core.Tokens;
using static Ether.Core.Common;

namespace Ether.Core.Checking.Inference
{
    public record InferenceCheckError(string message, IToken Location) : IResultError
    {
        public string Message => $"Error at line: {Location.Line}\n{message}";
    }
}