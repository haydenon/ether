using System;
using System.Collections.Generic;
using System.Linq;
using Ether.Core.Types;

namespace Ether.Core.Checking.Inference
{
    public class Context
    {
        private int next;
        private Dictionary<string, IType> env;
        // private List<TVariable> unresolved;

        private Context(int start, Dictionary<string, IType> env)
        {
            this.next = start;
            this.env = new Dictionary<string, IType>(env);
        }

        public Context(Dictionary<string, IType> defaultEnv) : this(0, defaultEnv) { }

        public static Context Empty => new Context(new Dictionary<string, IType>());

        public IType? this[string name] => this.env.ContainsKey(name) ? this.env[name] : null;

        public Context Clone()
            => new Context(next, env);

        public Context Add(IType type, string name)
        {
            var newCtx = this.Clone();
            newCtx.env[name] = type;
            return newCtx;
        }

        public Context MapTypes(Func<IType, IType> map)
        {
            var newCtx = this.Clone();
            foreach (var (name, type) in newCtx.env)
            {
                newCtx.env[name] = map(type);
            }
            return newCtx;
        }

        public Context AddRange(IEnumerable<(IType type, string name)> values)
        {
            var newCtx = this.Clone();
            foreach (var (type, name) in values)
            {
                newCtx.env[name] = type;
            }
            return newCtx;
        }
    }
}