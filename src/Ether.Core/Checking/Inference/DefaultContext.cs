using System.Collections.Generic;
using System.Linq;
using Ether.Core.Types;

namespace Ether.Core.Checking.Inference
{
    public static class DefaultContext
    {
        public static readonly Context Value = CreateDefaultContext();

        private static Context CreateDefaultContext()
        {
            var values = new List<(string, IType)>
            {
                AddBinaryFunc("$+_2", Primatives.NumType),
                AddBinaryFunc("$-_2", Primatives.NumType),
                AddBinaryFunc("$*_2", Primatives.NumType),
                AddBinaryFunc("$/_2", Primatives.NumType),
                AddUnaryFunc("$-_1", Primatives.NumType),
                ("clock", new TFunc(new [] {Primatives.UnitType, Primatives.NumType})),
            };

            return new Context(values.ToDictionary((nt) => nt.Item1, nt => nt.Item2));
        }

        public static (string, IType) AddBinaryFunc(string name, IType type)
            => (name, new TFunc(new[] { type, type, type }));

        public static (string, IType) AddUnaryFunc(string name, IType type)
            => (name, new TFunc(new[] { type, type }));
    }
}