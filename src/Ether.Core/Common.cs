using System;

namespace Ether.Core
{
    public class Common
    {
        public interface IResultError
        {
            string Message { get; }
        }

        public record GenericError(string Message) : IResultError
        {
        }

        public struct Result<T>
        {
            private Result(IResultError? error, T? value)
            {
                Error = error;
                Value = value;
                IsOk = error == null;
            }

            public bool IsOk;

            public IResultError? Error { get; }
            public T? Value { get; }

            public static Result<T> Ok(T value) => new Result<T>(default, value);

            public static Result<T> Err(IResultError error) => new Result<T>(error, default);

            public static Result<T> Err(string error) => new Result<T>(new GenericError(error), default);

            public Result<R> Bind<R>(Func<T, Result<R>> bind)
                => IsOk ? bind(Value!) : new Result<R>(Error, default);

            public Result<R> Map<R>(Func<T, R> mapper)
                => IsOk ? Result<R>.Ok(mapper(Value!)) : new Result<R>(Error, default);

            public R Match<R>(Func<T, R> match, Func<IResultError, R> matchErr)
                => IsOk ? match(Value!) : matchErr(Error!);

            public Result<T> PassIf<_>(Func<T, Result<_>> passIfOk)
                => this.Bind((val) => passIfOk(val).Map<T>((_ => val)));

            public Result<T> Tap(Action<T> action)
                => this.Map(val => { action(val); return val; });

            public Result<Unit> Ig()
                => this.Map(_ => unit);
        }

        public static Result<T> Ok<T>(T value) => Result<T>.Ok(value);
        public static Result<T> Err<T>(string error) => Result<T>.Err(error);
        public static Result<T> Err<T>(IResultError error) => Result<T>.Err(error);

        public class Unit
        {
            private Unit() { }

            private static readonly Unit unitInst = new Unit();

            public static Unit UnitInstance => unitInst;

            public static Unit Run<T>(Func<T> f)
            {
                f();
                return UnitInstance;
            }

            public static Unit Run(Action f)
            {
                f();
                return UnitInstance;
            }
        }

        public static Unit unit => Unit.UnitInstance;
        public static Unit Run<T>(Func<T> f) => Unit.Run(f);
        public static Unit Run(Action f) => Unit.Run(f);
    }
}