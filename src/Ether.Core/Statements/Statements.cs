using Ether.Core.Expressions;
using Ether.Core.Tokens;
using Ether.Core.Types;

namespace Ether.Core.Statements
{
    public record LetStmt(IToken Name, IExpr Value, IType? ExpectedType) : IStmt
    {
        public IToken RepresentingToken => Name;

        private string TypeAnnotationString =>
            ExpectedType != null ? $":{ExpectedType.ToString()}" : string.Empty;

        public override string ToString()
            => $"let {Name.Lexeme}{TypeAnnotationString} = {Value.ToString()};";
    }

    public record PrintStmt(IExpr Value) : IStmt
    {
        public IToken RepresentingToken => Value.RepresentingToken;

        public override string ToString()
            => $"print {Value.ToString()};";
    }

    public record ExprStmt(IExpr Value) : IStmt
    {
        public IToken RepresentingToken => Value.RepresentingToken;

        public override string ToString() => $"{Value.ToString() ?? string.Empty};";
    }


    public record ReturnStmt(IExpr Value) : IStmt
    {
        public IToken RepresentingToken => Value.RepresentingToken;

        public override string ToString() => $"return {Value.ToString() ?? string.Empty};";
    }

    public record ExprReturnStmt(IExpr Value) : ReturnStmt(Value)
    {
        public override string ToString() => Value.ToString() ?? string.Empty;
    }
}