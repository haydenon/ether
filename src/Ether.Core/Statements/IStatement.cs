using Ether.Core.Tokens;

namespace Ether.Core.Statements
{
    public interface IStmt
    {
        IToken RepresentingToken { get; }
    }
}