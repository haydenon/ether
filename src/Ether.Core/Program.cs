using System.Collections.Generic;
using Ether.Core.Statements;

namespace Ether.Core
{
    public record Program(List<IStmt> Statements);
}