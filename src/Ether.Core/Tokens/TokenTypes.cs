namespace Ether.Core.Tokens
{
    public enum TokenType
    {
        // Single character tokens
        LeftParen,
        RightParen,
        LeftBrace,
        RightBrace,
        Comma,
        Dot,
        Minus,
        Plus,
        SemiColon,
        Slash,
        Star,
        QuestionMark,
        Colon,
        Bang,
        LeftSquare,
        RightSquare,

        // One or two character tokens
        Equal,
        Arrow,
        Greater,
        GreaterEqual,
        Less,
        LessEqual,
        RocketShip,
        Pipe,
        Pipeline,

        // Literals
        Identifier,
        String,
        Number,

        // Keywords
        And,
        Else,
        False,
        Func,
        If,
        Match,
        Or,
        Print,
        Return,
        Then,
        This,
        True,
        Let,

        Eof
    }
}