namespace Ether.Core.Tokens
{
    public interface IToken
    {
        TokenType TokenType { get; }
        string Lexeme { get; }
        int Line { get; }
    }

    public record DataToken(TokenType TokenType, string Lexeme, int Line, object Data) : IToken;

    public record EmptyToken(TokenType TokenType, string Lexeme, int Line) : IToken;
}