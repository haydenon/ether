using System.Collections.Generic;
using Ether.Core.Tokens;

namespace Ether.Core.Interpreting
{
    public class Environment
    {
        private Environment? parent;

        private Dictionary<string, object> lookup = new Dictionary<string, object>();

        public Environment() { }

        public Environment(Environment parent)
        {
            this.parent = parent;
        }

        public void Add(IToken name, object value)
        {
            // TODO: allow shadowing
            if (lookup.ContainsKey(name.Lexeme))
            {
                throw new RuntimeException(name, $"Variable '{name.Lexeme}' is already defined");
            }

            lookup[name.Lexeme] = value;
        }

        public object Get(IToken name)
        {
            if (!lookup.ContainsKey(name.Lexeme))
            {
                return parent != null
                    ? parent.Get(name)
                    : throw new RuntimeException(name, $"Variable '{name.Lexeme}' is not defined");
            }

            return lookup[name.Lexeme];
        }
    }
}