namespace Ether.Core.Interpreting.Statements
{
    public class ReturnResultException : System.Exception
    {
        public object ReturnResult { get; }

        public ReturnResultException(object value)
        {
            ReturnResult = value;
        }
    };
}