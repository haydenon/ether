using Ether.Core.Tokens;
using static Ether.Core.Common;

namespace Ether.Core.Interpreting
{
    public static class Common
    {
        public static Unit RunError(IToken token, string message)
            => throw new RuntimeException(token, message);
    }
}