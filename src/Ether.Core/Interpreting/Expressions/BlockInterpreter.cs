using Ether.Core.Expressions;
using Ether.Core.Interpreting.Statements;
using static Ether.Core.Common;

namespace Ether.Core.Interpreting.Expressions
{
    public class BlockInterpreter
    {
        public static object Evaluate(Block block, Environment env, IInterpreter interpreter)
        {
            foreach (var stmt in block.Statements)
            {
                try
                {
                    interpreter.Evaluate(stmt, env);
                }
                catch (ReturnResultException ex)
                {
                    return ex.ReturnResult;
                }
            }

            return unit;
        }
    }
}