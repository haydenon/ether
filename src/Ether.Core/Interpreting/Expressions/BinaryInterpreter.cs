using System;
using Ether.Core.Expressions;
using Ether.Core.Tokens;

namespace Ether.Core.Interpreting.Expressions
{
    public static class BinaryInterpreter
    {
        public static object Evaluate(Binary expr, Environment env, IInterpreter interpreter)
        {
            var left = interpreter.Evaluate(expr.Left, env);
            var right = interpreter.Evaluate(expr.Right, env);

            return (expr.Op.TokenType, left, right) switch
            {
                (TokenType.Minus, double l, double r) => l - r,
                (TokenType.Plus, double l, double r) => l + r,
                (TokenType.Slash, double l, double r) => Divide(expr.Op, l, r),
                (TokenType.Star, double l, double r) => l * r,
                _ => throw new RuntimeException(expr.Op, "Invalid binary operation")
            };
        }

        private static double Divide(IToken op, double l, double r)
            => r != 0.0
                ? l / r
                : throw new RuntimeException(op, "Divide by zero error");
    }
}