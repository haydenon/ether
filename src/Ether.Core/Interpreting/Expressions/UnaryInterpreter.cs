using System;
using Ether.Core.Expressions;
using Ether.Core.Tokens;

namespace Ether.Core.Interpreting.Expressions
{
    public class UnaryInterpreter
    {
        public static object Evaluate(Unary expr, Environment env, IInterpreter interpreter)
        {
            var right = interpreter.Evaluate(expr.Right, env);

            return (expr.Op.TokenType, right) switch
            {
                (TokenType.Minus, double r) => -r,
                _ => throw new RuntimeException(expr.Op, "Invalid binary operation")
            };
        }
    }
}