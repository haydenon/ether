using System.Collections.Generic;
using System.Linq;
using Ether.Core.Expressions;

namespace Ether.Core.Interpreting.Expressions
{
    public static class CallInterpreter
    {
        public static object Evaluate(CallExpr call, Environment env, IInterpreter interpreter)
        {
            var callee = interpreter.Evaluate(call.Callee, env);
            if (callee is not InterpretFunction func)
            {
                throw new RuntimeException(call.RepresentingToken, "Can only call functions");
            }
            var (function, funcEnv) = func;
            var callEnv = new Environment(funcEnv);

            var args = call.Arguments.Select(a => interpreter.Evaluate(a, env)).ToArray();
            var argLength = args.Length;

            if (function.Parameters.Count > argLength)
            {
                var remaining = function.Parameters.Skip(argLength).ToArray();
                foreach (var (p, a) in function.Parameters.Take(argLength).Zip(args))
                {
                    callEnv.Add(p.Name, a);
                    var newFunc = new Function(remaining, function.Body, function.ReturnType);
                    return new InterpretFunction(newFunc, callEnv);
                }
            }

            return CallFunction(function, args, callEnv, interpreter);
        }

        private static object CallFunction(Function function, IList<object> args, Environment env, IInterpreter interpreter)
        {
            foreach (var (p, a) in function.Parameters.Zip(args))
            {
                env.Add(p.Name, a);
            }

            return interpreter.Evaluate(function.Body, env);
        }
    }
}