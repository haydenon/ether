using Ether.Core.Expressions;
using Ether.Core.Tokens;
using Ether.Core.Types;
using static Ether.Core.Common;

namespace Ether.Core.Interpreting.Expressions
{
    public static class IfInterpreter
    {
        private static IExpr UnitExpr(IToken location) => new Literal(location, unit, Primatives.UnitType);

        public static object Evaluate(IfExpr ifExpr, Environment env, IInterpreter interpreter)
        {
            var cond = interpreter.Evaluate(ifExpr.Condition, env);
            if (cond is not bool condBool)
            {
                throw new RuntimeException(ifExpr.Condition.RepresentingToken, "'if' condition should be a boolean");
            }


            return interpreter.Evaluate(condBool ? ifExpr.IfTrue : (ifExpr.IfFalse ?? UnitExpr(ifExpr.RepresentingToken)), env);
        }
    }
}