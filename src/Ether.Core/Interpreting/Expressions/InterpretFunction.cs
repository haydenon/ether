using Ether.Core.Expressions;

namespace Ether.Core.Interpreting.Expressions
{
    public record InterpretFunction(Function Function, Environment Env);
}