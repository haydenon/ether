using Ether.Core.Expressions;

namespace Ether.Core.Interpreting.Expressions
{
    public static class LiteralInterpreter
    {
        public static object Evaluate(Literal literal)
            => literal.Data;
    }
}