using System;
using Ether.Core.Expressions;
using Ether.Core.Checking.Inference;
using Ether.Core.Interpreting.Expressions;
using Ether.Core.Parsing;
using Ether.Core.Scanning;
using static Ether.Core.Common;
using static Ether.Core.Interpreting.Common;
using Ether.Core.Statements;
using Ether.Core.Interpreting.Statements;

namespace Ether.Core.Interpreting
{
    public class Interpreter : IInterpreter
    {
        private Interpreter() { }

        public static Result<Unit> Interpret(string program)
            => new Scanner(program).ScanTokens()
                .Bind(tokens => new Parser(tokens).ParseProgram())
                .PassIf(program => Inferer.InferProgram(program))
                .Bind(Interpret);

        public static Result<Unit> Interpret(Program program)
        {
            try
            {
                InterpretProgram(program);
                return Ok(unit);
            }
            catch (RuntimeException ex)
            {
                return Err<Unit>(ex.Message);
            }
        }

        private static void InterpretProgram(Program program)
        {
            var interpreter = new Interpreter();
            var globals = GlobalEnvironment.Value;

            var start = DateTime.UtcNow;
            foreach (var statement in program.Statements)
            {
                interpreter.Evaluate(statement, globals);
            }

            Console.WriteLine($"Took: {(DateTime.UtcNow - start).TotalMilliseconds}");
        }

        public object Evaluate(IStmt stmt, Environment env)
         => stmt switch
         {
             PrintStmt p => Print(p, env),
             LetStmt l => Let(l, env),
             ExprStmt e => RunExpr(e, env),
             ReturnStmt r => throw new ReturnResultException(Evaluate(r.Value, env)),
             _ => RunError(stmt.RepresentingToken, "Unexpected statement")
         };

        private Unit Print(PrintStmt print, Environment env)
        {
            var value = Evaluate(print.Value, env);
            Console.WriteLine(value);
            return unit;
        }

        private Unit RunExpr(ExprStmt expr, Environment env)
        {
            Evaluate(expr.Value, env);
            return unit;
        }

        private Unit Let(LetStmt let, Environment env)
        {
            var value = Evaluate(let.Value, env);
            env.Add(let.Name, value);
            return unit;
        }

        public object Evaluate(IExpr expr, Environment env)
            => expr switch
            {
                Binary bin => BinaryInterpreter.Evaluate(bin, env, this),
                Unary un => UnaryInterpreter.Evaluate(un, env, this),
                Literal lit => LiteralInterpreter.Evaluate(lit),
                Grouping grp => Evaluate(grp.Expr, env),
                Variable v => env.Get(v.Name),
                Function f => new InterpretFunction(f, env),
                CallExpr call => CallInterpreter.Evaluate(call, env, this),
                IfExpr ifExpr => IfInterpreter.Evaluate(ifExpr, env, this),
                Block block => BlockInterpreter.Evaluate(block, env, this),
                ReturnExpr { ReturnValue: IExpr value } => throw new ReturnResultException(Evaluate(value, env)),
                _ => RunError(expr.RepresentingToken, $"Unexpected expression '{expr}'")
            };
    }
}