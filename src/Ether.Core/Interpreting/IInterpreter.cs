using Ether.Core.Expressions;
using Ether.Core.Statements;

namespace Ether.Core.Interpreting
{
    public interface IInterpreter
    {
        object Evaluate(IExpr expr, Environment env);

        object? Evaluate(IStmt stmt, Environment env);
    }
}