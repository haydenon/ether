using Ether.Core.Tokens;

namespace Ether.Core.Interpreting
{
    public class RuntimeException : System.Exception
    {
        private readonly IToken token;

        public RuntimeException(IToken token, string message) : base(message)
        {
            this.token = token;
        }
    }
}