using System;
using System.Collections.Generic;
using System.Linq;
using Ether.Core.Expressions;
using Ether.Core.Statements;
using Ether.Core.Tokens;
using Ether.Core.Types;
using static Ether.Core.Common;

namespace Ether.Core.Parsing
{
    public class Parser : IParser
    {
        private readonly IList<IToken> tokens;
        private int current = 0;

        public Parser(IList<IToken> tokens)
        {
            this.tokens = tokens;
        }

        public Result<Program> ParseProgram()
        {
            var statements = new List<IStmt>();
            while (!IsAtEnd)
            {
                var statement = Statement();
                switch ((statement.Value, statement.Error))
                {
                    case (IStmt stmt, null):
                        statements.Add(stmt);
                        break;
                    case (_, IResultError err):
                        return Err<Program>(err);
                    default:
                        return Err<Program>("Failed to parse program");
                }
            }

            return Ok(new Program(statements));
        }

        private Result<IStmt> Statement(bool allowReturnExpression = false)
            => Next.TokenType switch
            {
                TokenType.Let => LetStatement(),
                TokenType.Print => PrintStatement(),
                TokenType.Return => ReturnStatement(),
                _ => ExprStatement(allowReturnExpression)
            };

        private Result<IStmt> ExprStatement(bool allowReturnExpression)
            => Expression()
                .Bind((expr) =>
                {
                    if (MatchType(TokenType.SemiColon))
                    {
                        return Ok(new ExprStmt(expr) as IStmt);
                    }
                    else if (!allowReturnExpression)
                    {
                        return Err<IStmt>(new ParseError(Previous, "Expect ';' after expression statement"));
                    }
                    else
                    {
                        return Ok(new ExprReturnStmt(expr) as IStmt);
                    }
                });

        private Result<IStmt> LetStatement()
            => Consume("Expect 'let' at start of let declaration", TokenType.Let)
                .Bind(_ => Consume("Expect variable name", TokenType.Identifier))
                .Bind(name => ExprTypeIfPresent($"Expected declared type for let declaration to variable {name}").Map(type => (name, type)))
                .PassIf((nt) => Consume($"Expect '=' after let declaration to variable '{nt.name}'", TokenType.Equal))
                .Bind((nt) => Expression().Map(expr => (nt.name, expr, nt.type)))
                .PassIf((nameExpr) => Consume($"Expect ';' after let assignment to variable '{nameExpr.name.Lexeme}'", TokenType.SemiColon))
                .Map<IStmt>((nt) => new LetStmt(nt.name, nt.expr, nt.type));

        private Result<IType?> ExprTypeIfPresent(string expectMessage)
        {
            if (!MatchType(TokenType.Colon))
            {
                return Ok<IType?>(null);
            }

            return ExprType(expectMessage)!;
        }

        private Result<IType> ExprType(string expectMessage)
        {
            if (MatchType(TokenType.Identifier))
            {
                return Ok((IType)new TPlain(Previous.Lexeme));
            }

            return FuncExprType(expectMessage);
        }

        private Result<IType> FuncExprType(string expectMessage)
        {
            if (!MatchType(TokenType.LeftParen))
            {
                return Err<IType>(new ParseError(Previous, expectMessage));
            }

            var parameters = new List<IType>();

            do
            {
                if (Check(TokenType.RightParen))
                {
                    break;
                }

                const string parameterTypeError = "Expected function parameter type";
                var parameter = ExprType(parameterTypeError);
                if (parameter is Result<IType?> { Value: IType type })
                {
                    parameters.Add(type);
                }
                else
                {
                    return Err<IType>(new ParseError(Previous, parameterTypeError));
                }
            } while (MatchType(TokenType.Comma));

            const string retTypeError = "Expected function return type";

            return Consume("Expect right parenthesis after function parameter types", TokenType.RightParen)
                .Bind(_ => Consume("Expected '=>' after function parameter types", TokenType.Arrow))
                .Bind(_ => ExprType(retTypeError))
                .Bind(retType =>
                {
                    if (retType == null)
                    {
                        return Err<IType>(new ParseError(Previous, retTypeError));
                    }

                    parameters.Add(retType);
                    return Ok<IType>(new TFunc(parameters));
                });
        }

        private Result<IStmt> PrintStatement()
            => Consume("Expect 'print' at start of print statement", TokenType.Print)
                .Bind(_ => Expression())
                .PassIf(_ => Consume("Expect ';' after print statement", TokenType.SemiColon))
                .Map<IStmt>(expr => new PrintStmt(expr));

        private Result<IStmt> ReturnStatement()
            => Consume("Expect 'return' at start of return statement", TokenType.Return)
                .Bind(_ => Expression())
                .PassIf(_ => Consume("Expect ';' after return statement", TokenType.SemiColon))
                .Map<IStmt>(expr => new ReturnStmt(expr));

        private Result<IExpr> Expression()
        {
            if (MatchType(TokenType.If))
            {
                return IfExpr();
            }

            return Term();
        }

        private Result<IExpr> IfExpr()
            => Expression()
                .PassIf(_ => Consume("Expect 'then' after if condition", TokenType.Then))
                .Bind(cond => ThenBranch().Map(trueBranch => (cond, trueBranch)))
                .Bind(condTrue => ElseBranch().Map<IExpr>(elseBranch => new IfExpr(condTrue.cond, condTrue.trueBranch, elseBranch)));

        private Result<IExpr> ThenBranch()
        {
            if (MatchType(TokenType.Return))
            {
                var ret = Previous;
                return Expression().Map(expr => new ReturnExpr(ret, expr) as IExpr);
            }

            return Expression();
        }
        private Result<IExpr?> ElseBranch()
        {
            if (!MatchType(TokenType.Else))
            {
                return Ok<IExpr?>(null);
            }

            if (MatchType(TokenType.Return))
            {
                var ret = Previous;
                return Expression().Map(expr => new ReturnExpr(ret, expr) as IExpr)!;
            }

            return Expression()!;
        }

        private Result<IExpr> Term()
            => BinaryExpr(() => Factor(), TokenType.Minus, TokenType.Plus);

        private Result<IExpr> Factor()
            => BinaryExpr(() => UnaryExpr(), TokenType.Slash, TokenType.Star);

        private Result<IExpr> BinaryExpr(Func<Result<IExpr>> exprFactory, params TokenType[] tokenTypes)
        {
            var exprResult = exprFactory();
            if (exprResult.Error is IResultError err)
            {
                return Err<IExpr>(err);
            }
            var expr = exprResult.Value!;

            while (MatchType(tokenTypes))
            {
                var op = Previous;
                var right = exprFactory();
                if (right.Error is IResultError rightErr)
                {
                    return Err<IExpr>(rightErr);
                }

                expr = new Binary(expr, op, right.Value!);
            }

            return Ok(expr);
        }

        private Result<IExpr> UnaryExpr()
        {
            if (MatchType(TokenType.Bang, TokenType.Minus))
            {
                var op = Previous;
                return UnaryExpr() switch
                {
                    { Value: IExpr right } => Ok<IExpr>(new Unary(op, right)),
                    { Error: IResultError err } => Err<IExpr>(err),
                    _ => Err<IExpr>("Invalid unary expression"),
                };
            }

            return Call();
        }

        private Result<IExpr> Call()
            => Primary().Bind(expr =>
            {
                while (true)
                {
                    if (MatchType(TokenType.LeftParen))
                    {
                        var res = FinishCall(expr);
                        if (res.Value is IExpr callExpr)
                        {
                            expr = callExpr;
                        }
                        else
                        {
                            return Err<IExpr>(res.Error!);
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                return Ok(expr);
            });


        private Result<IExpr> FinishCall(IExpr callee)
        {
            var args = new List<IExpr>();
            if (!Check(TokenType.RightParen))
            {
                do
                {
                    if (Check(TokenType.RightParen))
                    {
                        break;
                    }
                    var res = Expression();
                    if (res.Value is IExpr arg)
                    {
                        args.Add(arg);
                    }
                    else
                    {
                        return Err<IExpr>(res.Error!);
                    }
                } while (MatchType(TokenType.Comma));
            }

            return Consume("Expect ')' after arguments", TokenType.RightParen)
                .Map<IExpr>(paren => new CallExpr(callee, paren, args));
        }

        private Result<IExpr> Primary()
        {
            if (MatchType(TokenType.Number))
            {
                var literalToken = Previous;
                if (literalToken is DataToken token)
                {
                    return Ok<IExpr>(new Literal(token, token.Data, Primatives.NumType));
                }
                else
                {
                    return Err<IExpr>(new ParseError(literalToken, "Expected number literal"));
                }
            }

            if (MatchType(TokenType.True))
            {
                return Ok<IExpr>(new Literal(Previous, true, Primatives.BoolType));
            }
            if (MatchType(TokenType.False))
            {
                return Ok<IExpr>(new Literal(Previous, false, Primatives.BoolType));
            }

            if (MatchType(TokenType.LeftParen))
            {
                // TODO: Unit literal
                if (IsFunctionDefinition())
                {
                    return Function();
                }

                var exprResult = Term();
                if (exprResult.Error is IResultError err)
                {
                    return Err<IExpr>(err);
                }

                return Consume("Expect ')' after expression", TokenType.RightParen)
                    .Map(_ => new Grouping(exprResult.Value!) as IExpr);
            }

            if (MatchType(TokenType.Identifier))
            {
                return Ok<IExpr>(new Variable(Previous));
            }

            if (MatchType(TokenType.String))
            {
                var literalToken = Previous;
                if (literalToken is DataToken token)
                {
                    return Ok<IExpr>(new Literal(token, token.Data, Primatives.StringType));
                }
                else
                {
                    return Err<IExpr>(new ParseError(literalToken, "Expected string literal"));
                }
            }

            if (MatchType(TokenType.LeftSquare))
            {
                return Array();
            }

            var next = Next;
            return Err<IExpr>(new ParseError(next, Messages.ExpectedExpression(next)));
        }

        private bool IsFunctionDefinition()
        {
            /*
                If the following token is an identifier, it's a function definition if:
                  a) it's followed by a comma - a list of parameters
                  b) it's followed by a type annotation
                  c) it's a single parameter with no trailing comma or type annotation, but has a closing parenthesis and arrow
                Else, if there are no parameters, it's different to a unit literal if it has an arrow or return annotation afterwards
            */
            var nextToken = 0;
            if (Check(TokenType.Identifier))
            {
                if (Check(TokenType.Comma, 1) || Check(TokenType.Colon, 1))
                {
                    return true;
                }

                nextToken++;
            }

            return Check(TokenType.RightParen, nextToken) &&
                (Check(TokenType.Arrow, nextToken + 1) || Check(TokenType.Colon, nextToken + 1));
        }

        private Result<IExpr> Function()
        {
            var opening = Previous;
            var parameters = new List<FunctionParameter>();
            do
            {
                if (Check(TokenType.RightParen))
                {
                    break;
                }

                if (!MatchType(TokenType.Identifier))
                {
                    return Err<IExpr>(new ParseError(Next, "Expected parameter name"));
                }
                var parameterName = Previous;
                if (parameters.Any(p => p.Name.Lexeme == parameterName.Lexeme))
                {
                    return Err<IExpr>(new ParseError(parameterName, Messages.DuplicateParameter(parameterName.Lexeme)));
                }
                ExprTypeIfPresent($"Expected declared type for parameter {parameterName}")
                    .Tap(type => parameters.Add(new FunctionParameter(parameterName, type)));
            } while (MatchType(TokenType.Comma));

            if (!MatchType(TokenType.RightParen))
            {
                return Err<IExpr>(new ParseError(Next, "Expected ')' after function parameters"));
            }
            if (!parameters.Any())
            {
                var unitParam = new EmptyToken(TokenType.Identifier, "$", opening.Line);
                parameters.Add(new FunctionParameter(unitParam, new TPlain("Unit")));
            }

            return ExprTypeIfPresent("Expected return type for function")
                .PassIf(_ => Consume("Expected function arrow after parameter list", TokenType.Arrow))
                .Bind(type => FunctionBody().Map(expr => (expr, type)))
                .Map<IExpr>(exprType => new Function(parameters, exprType.expr, exprType.type));
        }

        private Result<IExpr> FunctionBody()
        {
            if (MatchType(TokenType.LeftBrace))
            {
                var statements = new List<IStmt>();
                do
                {
                    switch (Statement(allowReturnExpression: true))
                    {
                        case Result<IStmt> { Value: IStmt statement }:
                            statements.Add(statement);
                            break;
                        case Result<IStmt> { Error: IResultError err }:
                            return Err<IExpr>(err);
                        default:
                            return Err<IExpr>(new ParseError(Previous, "Invalid block"));
                    }
                } while (!MatchType(TokenType.RightBrace));

                var returnExprStmtInd = statements.FindIndex(stmt => stmt is ReturnStmt);
                if (returnExprStmtInd != -1 && returnExprStmtInd != statements.Count - 1)
                {
                    var expr = statements[returnExprStmtInd];
                    var message = expr is ExprReturnStmt
                        ? Messages.ExpressionCanOnlyBeLastBlockItem
                        : Messages.UnreachableCodeAfterReturn;
                    return Err<IExpr>(
                        new ParseError(expr.RepresentingToken, message));
                }

                return Ok(new Block(statements) as IExpr);
            }

            return Expression();
        }

        private Result<IExpr> Array()
        {
            var opening = Previous;
            var values = new List<IExpr>();

            do
            {
                if (MatchType(TokenType.RightSquare))
                {
                    return Ok<IExpr>(new ArrayConstructor(opening, values, null));
                }

                switch (Expression())
                {
                    case Result<IExpr> { Value: IExpr val }:
                        values.Add(val);
                        break;
                    case Result<IExpr> { Error: IResultError err }:
                        return Err<IExpr>(err);
                    default:
                        return Err<IExpr>(new ParseError(Previous, "Invalid array value"));
                }

            } while (MatchType(TokenType.Comma));

            return Consume("Expect ']' after array literal", TokenType.RightSquare)
                    .Map<IExpr>(_ => new ArrayConstructor(opening, values, null));
        }

        private bool MatchType(params TokenType[] types)
        {
            foreach (var tokenType in types)
            {
                if (Check(tokenType))
                {
                    Advance();
                    return true;
                }
            }

            return false;
        }

        private bool Check(TokenType tokenType, int position = 0)
        {
            for (var i = 0; i < position; i++)
            {
                if (tokens[current + i].TokenType == TokenType.Eof)
                {
                    return false;
                }
            }

            return tokens[current + position].TokenType == tokenType;
        }

        private IToken Advance()
        {
            if (!IsAtEnd)
            {

                current++;
            }

            return Previous;
        }

        private Result<IToken> Consume(string message, params TokenType[] types)
        {
            foreach (var tokenType in types)
            {
                if (Check(tokenType))
                {
                    return Ok(Advance());
                }
            }

            return Err<IToken>(new ParseError(Previous, message));
        }

        private IToken Next => tokens[current];

        private IToken Previous => current <= tokens.Count ? tokens[current - 1] : CreateEof();

        private bool IsAtEnd => Next.TokenType == TokenType.Eof;

        private IToken CreateEof()
            => (tokens.Any())
                ? new EmptyToken(TokenType.Eof, "", tokens[tokens.Count - 1].Line)
                : new EmptyToken(TokenType.Eof, "", 0);
    }

    public record ParseError(IToken Token, string Message) : IResultError;
}