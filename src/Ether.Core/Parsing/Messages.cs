using Ether.Core.Tokens;

namespace Ether.Core.Parsing
{
    public class Messages
    {
        public static string ExpectedExpression(IToken token) => $"Expected expression at line {token.Line}: '{token.Lexeme}'";

        #region Functions

        public static string DuplicateParameter(string name) => $"Duplicate parameter {name} in function declaration";

        public const string ExpressionCanOnlyBeLastBlockItem = "Only the last item in a block can be a return expression";

        public const string UnreachableCodeAfterReturn = "Can't have unreachable code after return statement";

        #endregion
    }
}