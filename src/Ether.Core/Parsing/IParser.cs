using static Ether.Core.Common;

namespace Ether.Core.Parsing
{
    public interface IParser
    {
        Result<Program> ParseProgram();
    }
}