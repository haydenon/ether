using System.Collections.Generic;
using System.Linq;

namespace Ether.Core.Types
{
    public interface IType
    {
        string Name { get; }
    }

    public interface ITConstructor : IType
    {
        IList<IType> Generics { get; }
        void Deconstruct(out string name, out IList<IType> generics);
    }

    public record TConstructor(string Name, IList<IType> Generics) : ITConstructor
    {
        public override string ToString() => $"{Name}<{string.Join(", ", Generics.Select(g => g.ToString()))}>";

        public void Deconstruct(out string name, out IList<IType> generics)
        {
            name = Name;
            generics = Generics;
        }
    }

    public record TPlain(string Name) : ITConstructor
    {
        public IList<IType> Generics => new IType[0];

        public void Deconstruct(out string name, out IList<IType> generics)
        {
            name = this.Name;
            generics = new IType[0];
        }

        public override string ToString() => Name;
    }

    public record TFunc(IList<IType> generics) : ITConstructor
    {
        private readonly string name = $"Func{generics.Count - 1}";
        public string Name => name;
        public IList<IType> Generics => generics;
        public override string ToString() => $"({string.Join(", ", generics.Take(generics.Count - 1).Select(g => g.ToString()))}) => {generics.Last().ToString()}";

        public void Deconstruct(out string name, out IList<IType> generics)
        {
            name = Name;
            generics = this.Generics;
        }
    }

    public record TVariable(int index) : IType
    {
        public override string ToString() => $"${index}";
        public string Name => this.ToString();
    }
}