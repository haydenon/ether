namespace Ether.Core.Types
{
    public static class Primatives
    {
        public static readonly IType UnitType = new TPlain("Unit");
        public static readonly IType NumType = new TPlain("Num");
        public static readonly IType StringType = new TPlain("String");
        public static readonly IType BoolType = new TPlain("Bool");
    }
}