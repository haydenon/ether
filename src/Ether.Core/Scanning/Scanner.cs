using System;
using System.Collections.Generic;
using System.Linq;
using Ether.Core.Tokens;
using static Ether.Core.Common;
using static Ether.Core.Tokens.TokenType;

namespace Ether.Core.Scanning
{
    public class Scanner : IScanner
    {
        private static readonly Dictionary<string, TokenType> Keywords = new Dictionary<string, TokenType>
        {
            { "and", And },
            { "else", Else },
            { "false", False },
            { "func", Func },
            { "if", If },
            { "match", Match },
            { "or", Or },
            { "print", Print },
            { "return", Return },
            { "then", Then },
            { "this", This },
            { "true", True },
            { "let", Let },
        };

        private string source;
        private int start;
        private int current;
        private int line = 1;
        private List<ScanError> errors = new List<ScanError>();
        private List<IToken> tokens = new List<IToken>();

        public Scanner(string source)
        {
            this.source = source;
        }

        public Result<List<IToken>> ScanTokens()
        {
            while (!IsAtEnd)
            {
                start = current;
                ScanToken();
            }

            tokens.Add(new EmptyToken(Eof, "", line));

            return errors.Any()
                ? Result<List<IToken>>.Err(errors.First())
                : Result<List<IToken>>.Ok(tokens);
        }

        private void ScanToken()
        {
            var c = Advance();
            _ = c switch
            {
                '(' => AddToken(LeftParen),
                ')' => AddToken(RightParen),
                '{' => AddToken(LeftBrace),
                '}' => AddToken(RightBrace),
                ',' => AddToken(Comma),
                '.' => AddToken(Dot),
                '-' => AddToken(Minus),
                '+' => AddToken(Plus),
                ';' => AddToken(SemiColon),
                '*' => AddToken(Star),
                '?' => AddToken(QuestionMark),
                ':' => AddToken(Colon),
                '!' => AddToken(Bang),
                '[' => AddToken(LeftSquare),
                ']' => AddToken(RightSquare),
                '=' => AddToken(MatchChar('>') ? Arrow : Equal),
                '<' => AddToken(MatchChar('=') ? LessEqual : (MatchChar('>') ? RocketShip : Less)),
                '>' => AddToken(MatchChar('=') ? GreaterEqual : Greater),
                '|' => AddToken(MatchChar('>') ? Pipeline : Pipe),
                '/' => HandleSlash(),
                ' ' => unit,
                '\r' => unit,
                '\t' => unit,
                '\n' => Run(() => line++),
                '"' => HandleString(),
                >= '0' and <= '9' => HandleNumber(),
                (>= 'a' and <= 'z') or (>= 'A' and <= 'Z') => HandleIdentifier(),
                _ => Run(() => errors.Add(new ScanError(line, $"Unexpected character: '{c}'")))
            };
        }

        public Unit HandleNumber()
        {
            while (IsDigit(NextChar))
            {
                Advance();
            }

            // Look for a fractional part.
            if (NextChar == '.' && IsDigit(NextNextChar))
            {
                // Consume the "."
                Advance();
            }

            while (IsDigit(NextChar))
            {
                Advance();
            }

            AddToken(Number, double.Parse(source.Substring(start, StrEnd)));
            return unit;
        }

        private bool IsDigit(char c) => c is >= '0' and <= '9';

        public Unit HandleIdentifier()
        {
            while (IsAlphaNumeric(NextChar))
            {
                Advance();
            }

            var text = source.Substring(start, StrEnd);
            var type = Keywords.ContainsKey(text) ? Keywords[text] : Identifier;
            AddToken(type);

            return unit;
        }

        private bool IsAlphaNumeric(char c) => IsDigit(c) || c is (>= 'a' and <= 'z') or (>= 'A' and <= 'Z');

        private Unit HandleString()
        {
            var startLine = line;
            while (NextChar != '"' && !IsAtEnd)
            {
                if (NextChar == '\n')
                {
                    line++;
                }
                Advance();
            }

            if (IsAtEnd)
            {
                errors.Add(new ScanError(line, "Unterminated string."));
                return unit;
            }

            Advance();

            var value = source.Substring(start + 1, StrEnd - 2);
            AddToken(TokenType.String, value, startLine);

            return unit;
        }

        private Unit HandleSlash()
        {
            if (MatchChar('/'))
            {
                while (NextChar != '\n' && !IsAtEnd)
                {
                    Advance();
                }
            }
            else if (MatchChar('*'))
            {
                MultiLineComment();
            }
            else
            {
                AddToken(Slash);
            }

            return unit;
        }

        private void MultiLineComment()
        {
            var level = 1;
            while (level != 0 && !IsAtEnd)
            {
                if (MatchChar('/') && MatchChar('*'))
                {
                    level++;
                }
                else if (MatchChar('*') && MatchChar('/'))
                {
                    level--;
                }
                else if (MatchChar('\n'))
                {
                    line++;
                }
                else
                {
                    Advance();
                }
            }

            if (level != 0)
            {
                errors.Add(new ScanError(line, "Expected multi-line comment to be terminated"));
            }
        }

        private char Advance()
        {
            current++;
            return source[current - 1];
        }

        private bool MatchChar(char expected)
        {
            if (IsAtEnd || NextChar != expected)
            {
                return false;
            }

            current++;
            return true;
        }

        private char NextChar => IsAtEnd ? '\u0000' : source[current];

        private char NextNextChar => current + 1 >= source.Length ? '\u0000' : source[current + 1];

        private bool IsAtEnd
            => current >= source.Length;

        private Unit AddToken(TokenType tokenType)
        {
            var text = source.Substring(start, StrEnd);
            tokens.Add(new EmptyToken(tokenType, text, line));
            return unit;
        }

        private Unit AddToken(TokenType tokenType, object data, int? specificLine = null)
        {
            var text = source.Substring(start, StrEnd);
            tokens.Add(new DataToken(tokenType, text, specificLine ?? line, data));
            return unit;
        }

        private int StrEnd => current - start;
    }

    public record ScanError(int Line, string Message) : IResultError;
}