using System.Collections.Generic;
using static Ether.Core.Common;
using Ether.Core.Tokens;

namespace Ether.Core.Scanning
{
    public interface IScanner
    {
        Result<List<IToken>> ScanTokens();
    }
}