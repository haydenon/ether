﻿using System;
using System.IO;
using Ether.Core.Interpreting;
using static Ether.Core.Common;

namespace Ether.Cli
{
    public class Entry
    {
        public static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                throw new InvalidOperationException("Must provide a file name to run");
            }
            var code = LoadCode(args[0]);

            var result = Interpreter.Interpret(code);
            if (result.Error is IResultError resultError)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine(resultError.Message);
                Console.ResetColor();
                System.Environment.Exit(1);
            }
        }

        private static string LoadCode(string fileName)
            => File.ReadAllText(fileName);
    }
}
