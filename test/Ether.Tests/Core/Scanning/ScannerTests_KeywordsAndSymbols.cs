using Ether.Core.Scanning;
using Ether.Core.Tokens;
using FluentAssertions;
using Xunit;
using static Ether.Core.Tokens.TokenType;
using static Ether.Tests.Core.TokenTestHelpers;

namespace Ether.Tests.Core.Scanning
{
    public class ScannerTests_KeywordsAndSymbols
    {
        private Scanner ClassUnderTest(string program) => new Scanner(program);


        [Theory]
        [InlineData("and", And)]
        [InlineData("else", Else)]
        [InlineData("false", False)]
        [InlineData("func", Func)]
        [InlineData("if", If)]
        [InlineData("match", Match)]
        [InlineData("or", Or)]
        [InlineData("print", Print)]
        [InlineData("return", Return)]
        [InlineData("then", Then)]
        [InlineData("this", This)]
        [InlineData("true", True)]
        [InlineData("let", Let)]
        public void Given_keywords_then_parses_correctly(string keyword, TokenType type)
        {
            // Arrange
            var cot = ClassUnderTest(keyword);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls(
                Emp(type, keyword)
            ));
        }

        [Theory]
        [InlineData("(", LeftParen)]
        [InlineData(")", RightParen)]
        [InlineData("{", LeftBrace)]
        [InlineData("}", RightBrace)]
        [InlineData(",", Comma)]
        [InlineData(".", Dot)]
        [InlineData("-", Minus)]
        [InlineData("+", Plus)]
        [InlineData(";", SemiColon)]
        [InlineData("*", Star)]
        [InlineData("?", QuestionMark)]
        [InlineData(":", Colon)]
        [InlineData("!", Bang)]
        [InlineData("[", LeftSquare)]
        [InlineData("]", RightSquare)]
        [InlineData("=", Equal)]
        [InlineData("=>", Arrow)]
        [InlineData("<", Less)]
        [InlineData("<=", LessEqual)]
        [InlineData("<>", RocketShip)]
        [InlineData(">", Greater)]
        [InlineData(">=", GreaterEqual)]
        [InlineData("|>", Pipeline)]
        [InlineData("|", Pipe)]
        [InlineData("/", Slash)]
        public void Given_symbols_then_parses_correctly(string symbol, TokenType type)
        {
            // Arrange
            var cot = ClassUnderTest(symbol);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls(
                Emp(type, symbol)
            ));
        }
    }
}