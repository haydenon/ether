using Ether.Core.Scanning;
using FluentAssertions;
using Xunit;
using static Ether.Core.Tokens.TokenType;
using static Ether.Tests.Core.TokenTestHelpers;

namespace Ether.Tests.Core.Scanning
{
    public class ScannerTests_Strings
    {
        private Scanner ClassUnderTest(string program) => new Scanner(program);

        [Fact]
        public void Given_valid_string_then_returns_string_token()
        {
            // Arrange
            var str = "Here is a string 1234";
            var expression = $"\"{str}\"";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls(
                Dat(String, expression, str)
            ));
        }

        [Fact]
        public void Given_unterminated_string_then_return_error()
        {
            // Arrange
            var str = "Here is a string 1234";
            var expression = $"\"{str}";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Error.Should().NotBeNull();
            tokens.Error!.Message.Should().Be("Unterminated string.");
        }

        [Fact]
        public void Given_valid_multi_line_string_then_returns_string_token()
        {
            // Arrange
            var str = "Here\nis\na\nstring";
            var expression = $"\"{str}\"";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls(
                Dat(String, expression, "Here\nis\na\nstring"),
                Emp(Eof, "", 4)
            ));
        }

        [Fact]
        public void Given_unterminated_multi_line_string_then_return_error()
        {
            // Arrange
            var str = "Here\nis\na\nstring";
            var expression = $"\"{str}";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Error.Should().NotBeNull();
            tokens.Error!.Message.Should().Be("Unterminated string.");
        }
    }
}