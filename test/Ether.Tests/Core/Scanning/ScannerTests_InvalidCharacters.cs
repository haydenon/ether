using Ether.Core.Scanning;
using FluentAssertions;
using Xunit;

namespace Ether.Tests.Core.Scanning
{
    public class ScannerTests_InvalidCharacters
    {
        private Scanner ClassUnderTest(string program) => new Scanner(program);

        [Theory]
        [InlineData("#")]
        [InlineData("&")]
        [InlineData("@")]
        public void Given_invalid_characters_then_returns_error(string character)
        {
            // Arrange
            var cot = ClassUnderTest(character);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Error.Should().NotBe(null);
            tokens.Error!.Message.Should().Be($"Unexpected character: '{character}'");
        }
    }
}