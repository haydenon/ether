using Ether.Core.Scanning;
using FluentAssertions;
using Xunit;
using static Ether.Core.Tokens.TokenType;
using static Ether.Tests.Core.TokenTestHelpers;

namespace Ether.Tests.Core.Scanning
{
    public class ScannerTests_Whitespace
    {
        private Scanner ClassUnderTest(string program) => new Scanner(program);

        [Theory]
        [InlineData("let      b    =\t6;")]
        [InlineData("let b=6;")]
        public void Given_various_whitespace_then_parses_correctly(string expression)
        {
            // Arrange
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls(
                Emp(Let, "let"),
                Emp(Identifier, "b"),
                Emp(Equal, "="),
                Dat(Number, "6", 6),
                Emp(SemiColon, ";")
            ));
        }

        [Fact]
        public void Given_new_lines_then_changes_line()
        {
            // Arrange
            var expression = "1\n+\n\n2";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls(
                Dat(Number, "1", 1, 1),
                Emp(Plus, "+", 2),
                Dat(Number, "2", 2, 4)
            ));
        }
    }
}