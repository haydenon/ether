using Ether.Core.Scanning;
using FluentAssertions;
using Xunit;
using static Ether.Core.Tokens.TokenType;
using static Ether.Tests.Core.TokenTestHelpers;

namespace Ether.Tests.Core.Scanning
{
    public class ScannerTests_Comments
    {
        private Scanner ClassUnderTest(string program) => new Scanner(program);

        [Fact]
        public void Given_comment_after_expression_then_retains_expression()
        {
            // Arrange
            var expression = "let b = 6; // Here is a comment";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls(
                Emp(Let, "let"),
                Emp(Identifier, "b"),
                Emp(Equal, "="),
                Dat(Number, "6", 6),
                Emp(SemiColon, ";")
            ));
        }

        [Fact]
        public void Given_comment_before_expression_then_does_not_scan_expression()
        {
            // Arrange
            var expression = "// Here is a comment // let b = 6;";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls());
        }

        [Fact]
        public void Given_multi_line_comment_after_expression_then_retains_expression()
        {
            // Arrange
            var expression = "let b = 6; /* Here is a comment */";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls(
                Emp(Let, "let"),
                Emp(Identifier, "b"),
                Emp(Equal, "="),
                Dat(Number, "6", 6),
                Emp(SemiColon, ";")
            ));
        }

        [Fact]
        public void Given_multi_line_comment_ending_before_expression_then_retains_expression()
        {
            // Arrange
            var expression = "/* Here is a comment */ let b = 6;";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls(
                Emp(Let, "let"),
                Emp(Identifier, "b"),
                Emp(Equal, "="),
                Dat(Number, "6", 6),
                Emp(SemiColon, ";")
            ));
        }

        [Fact]
        public void Given_multi_line_comment_over_multiple_lines_then_is_valid()
        {
            // Arrange
            var expression = "let /* Here\nis\na\ncomment */ let";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls(
                Emp(Let, "let"),
                Emp(Let, "let", 4)
            ));
        }


        [Fact]
        public void Given_unterminated_multi_line_comment_then_return_error()
        {
            // Arrange
            var expression = "let /* Here\nis\na\ncomment";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Error.Should().NotBeNull();
            tokens.Error!.Message.Should().Be("Expected multi-line comment to be terminated");
        }
    }
}