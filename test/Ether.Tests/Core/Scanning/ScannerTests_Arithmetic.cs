using Ether.Core.Scanning;
using Ether.Core.Tokens;
using FluentAssertions;
using Xunit;
using static Ether.Core.Tokens.TokenType;
using static Ether.Tests.Core.TokenTestHelpers;

namespace Ether.Tests.Core.Scanning
{
    public class ScannerTests_Arithmetic
    {
        private Scanner ClassUnderTest(string program) => new Scanner(program);

        [Theory]
        [InlineData("+", Plus)]
        [InlineData("-", Minus)]
        [InlineData("*", Star)]
        [InlineData("/", Slash)]
        public void Given_different_operators_then_parses_correctly(string op, TokenType type)
        {
            // Arrange
            var expression = $"1 {op} 2";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls(
                Dat(Number, "1", 1),
                Emp(type, op),
                Dat(Number, "2", 2)
            ));
        }

        [Fact]
        public void Given_complex_expressions_then_parses_correctly()
        {
            // Arrange
            var expression = "1 + 2 / 5";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls(
                Dat(Number, "1", 1),
                Emp(Plus, "+"),
                Dat(Number, "2", 2),
                Emp(Slash, "/"),
                Dat(Number, "5", 5)
            ));
        }

        [Fact]
        public void Given_groupings_then_parses_correctly()
        {
            // Arrange
            var expression = "(1 + 2) / 5";
            var cot = ClassUnderTest(expression);

            // Act
            var tokens = cot.ScanTokens();

            // Assert
            tokens.Value.Should().BeEquivalentTo(Ls(
                Emp(LeftParen, "("),
                Dat(Number, "1", 1),
                Emp(Plus, "+"),
                Dat(Number, "2", 2),
                Emp(RightParen, ")"),
                Emp(Slash, "/"),
                Dat(Number, "5", 5)
            ));
        }
    }
}