using System.Linq;
using Ether.Core.Expressions;
using Ether.Core.Statements;
using Ether.Core.Tokens;
using Ether.Core.Types;

namespace Ether.Tests.Core
{
    public static class ExprTestHelpers
    {
        public static Literal CreateLiteral(string type)
            => new Literal(new DataToken(TokenType.Plus, "", 1, 1), "1", new TPlain(type));

        public static Literal CreateLiteral(IToken token, IType type, object data)
            => new Literal(token, data, type);

        public static ReturnExpr CreateReturnExpr(IExpr expr)
            => new ReturnExpr(new EmptyToken(TokenType.Return, "return", 1), expr);

        public static Function CreateFunc(IExpr expr, IType type = null)
            => new Function(new FunctionParameter[] { new FunctionParameter(new EmptyToken(TokenType.Identifier, "$", 1), new TPlain("Unit")) }, expr, type);

        public static Variable CreateVariable(string name)
            => new Variable(new EmptyToken(TokenType.Identifier, name, 1));

        public static Block CreateBlock(params IStmt[] stmts)
            => new Block(stmts.ToList());
    }
}