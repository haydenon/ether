using System.Linq;
using Ether.Core;
using Ether.Core.Expressions;
using Ether.Core.Statements;
using FluentAssertions;
using static Ether.Core.Common;

namespace Ether.Tests.Core.Parsing
{
    public static class ParserTestHelpers
    {
        public static IExpr AsExpr(this Result<Program> result)
        {
            var exprStatement = result.Value?.Statements?.FirstOrDefault() as ExprStmt;
            exprStatement.Should().NotBeNull($"Expected an expression statement, but was an error: {result.Error?.Message}");
            return exprStatement.Value;
        }

        public static Function AsFunc(this Result<Program> result)
            => ((result.Value?.Statements?.FirstOrDefault() as ExprStmt).Value) as Function;

        public static Block GetFuncBody(this Result<Program> result)
            => (((result.Value?.Statements?.FirstOrDefault() as ExprStmt).Value) as Function)?.Body as Block;

        public static IfExpr AsIf(this Result<Program> result)
            => ((result.Value?.Statements?.FirstOrDefault() as ExprStmt).Value) as IfExpr;
    }
}