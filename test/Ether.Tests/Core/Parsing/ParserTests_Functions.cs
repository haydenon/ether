using System.Collections.Generic;
using Ether.Core.Expressions;
using Ether.Core.Parsing;
using Ether.Core.Tokens;
using Ether.Core.Types;
using FluentAssertions;
using Xunit;
using static Ether.Tests.Core.TokenTestHelpers;

namespace Ether.Tests.Core.Parsing
{
    public class ParserTests_Functions
    {
        private static Parser ClassUnderTest(IList<IToken> tokens) => new Parser(tokens);

        #region Parameters

        [Fact]
        public void Given_parameterless_function_definition_then_returns_expression()
        {
            // Arrange
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Dat(TokenType.Number, "1", 1),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<Function>();
            var func = result.AsFunc();
            func.Parameters.Should().Equal(new[] { FunctionParameter.UnitParameter(1) });
        }

        [Fact]
        public void Given_function_with_parameters_definition_then_returns_valid_function()
        {
            // Arrange
            var a = Emp(TokenType.Identifier, "a");
            var b = Emp(TokenType.Identifier, "b");
            var c = Emp(TokenType.Identifier, "c");
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                a,
                Emp(TokenType.Comma, ","),
                b,
                Emp(TokenType.Comma, ","),
                c,
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Dat(TokenType.Number, "1", 1),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<Function>();
            var func = result.AsFunc();
            func.Parameters.Should().Equal(new[] {
                new FunctionParameter(a, null),
                new FunctionParameter(b, null),
                new FunctionParameter(c, null),
            });
        }

        [Fact]
        public void Given_function_with_no_comma_between_parameters_then_returns_errors()
        {
            // Arrange
            var a = Emp(TokenType.Identifier, "a");
            var b = Emp(TokenType.Identifier, "b");
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                a,
                b,
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Dat(TokenType.Number, "1", 1),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.Error.Should().NotBeNull();
        }

        [Fact]
        public void Given_function_with_trailing_comma_then_returns_valid_function()
        {
            // Arrange
            var a = Emp(TokenType.Identifier, "a");
            var b = Emp(TokenType.Identifier, "b");
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                a,
                Emp(TokenType.Comma, ","),
                b,
                Emp(TokenType.Comma, ","),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Dat(TokenType.Number, "1", 1),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<Function>();
            var func = result.AsFunc();
            func.Parameters.Should().Equal(new[] {
                new FunctionParameter(a, null),
                new FunctionParameter(b, null),
            });
        }

        [Fact]
        public void Given_function_with_parameter_type_then_returns_valid_function()
        {
            // Arrange
            var a = Emp(TokenType.Identifier, "a");
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                a,
                Emp(TokenType.Colon, ":"),
                Emp(TokenType.Identifier, "Bool"),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Dat(TokenType.Number, "1", 1),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<Function>();
            var func = result.AsFunc();
            func.Parameters.Should().Equal(new[] {
                new FunctionParameter(a, Primatives.BoolType),
            });
        }

        [Fact]
        public void Given_function_with_just_parameter_type_then_returns_error()
        {
            // Arrange
            var a = Emp(TokenType.Identifier, "a");
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                Emp(TokenType.Colon, ":"),
                Emp(TokenType.Identifier, "Bool"),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Dat(TokenType.Number, "1", 1),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.Error.Message.Should().NotBeNull();
        }

        [Fact]
        public void Given_function_with_mix_of_parameter_type_and_no_type_then_returns_valid_function()
        {
            // Arrange
            var a = Emp(TokenType.Identifier, "a");
            var b = Emp(TokenType.Identifier, "b");
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                a,
                Emp(TokenType.Comma, ","),
                b,
                Emp(TokenType.Colon, ":"),
                Emp(TokenType.Identifier, "String"),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Dat(TokenType.Number, "1", 1),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<Function>();
            var func = result.AsFunc();
            func.Parameters.Should().Equal(new[] {
                new FunctionParameter(a, null),
                new FunctionParameter(b, Primatives.StringType),
            });
        }

        [Fact]
        public void Given_function_with_duplicate_parameter_then_returns_error()
        {
            // Arrange
            var a = Emp(TokenType.Identifier, "a");
            var b = Emp(TokenType.Identifier, "a");
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                a,
                Emp(TokenType.Comma, ","),
                b,
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Dat(TokenType.Number, "1", 1),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.Error.Message.Should().Be(Messages.DuplicateParameter(a.Lexeme));
        }

        #endregion


        #region Function type

        [Fact]
        public void Given_function_without_return_type_then_returns_valid_function()
        {
            // Arrange
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Dat(TokenType.Number, "1", 1),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<Function>();
            var func = result.AsFunc();
            func.ReturnType.Should().BeNull();
        }

        [Fact]
        public void Given_function_with_return_type_then_returns_valid_function()
        {
            // Arrange
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Colon, ":"),
                Emp(TokenType.Identifier, "Num"),
                Emp(TokenType.Arrow, "=>"),
                Dat(TokenType.Number, "1", 1),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<Function>();
            var func = result.AsFunc();
            func.ReturnType.Should().Be(Primatives.NumType);
        }

        #endregion

        #region Function body

        [Fact]
        public void Given_function_with_expression_body_then_returns_valid_function()
        {
            // Arrange
            var bodyValue = "hello world";
            var bodyToken = Dat(TokenType.String, "\"hello world\"", bodyValue);
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                bodyToken,
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<Function>();
            var func = result.AsFunc();
            func.Body.Should().Be(new Literal(bodyToken, bodyValue, Primatives.StringType));
        }

        [Fact]
        public void Given_function_without_expression_body_then_returns_error()
        {
            // Arrange
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.Error.Message.Should().Be(Messages.ExpectedExpression(Semi()));
        }

        #endregion
    }
}