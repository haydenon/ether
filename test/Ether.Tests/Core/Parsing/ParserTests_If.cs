using System.Collections.Generic;
using Ether.Core.Expressions;
using Ether.Core.Parsing;
using Ether.Core.Tokens;
using Ether.Core.Types;
using FluentAssertions;
using Xunit;
using static Ether.Tests.Core.ExprTestHelpers;
using static Ether.Tests.Core.TokenTestHelpers;

namespace Ether.Tests.Core.Parsing
{
    public class ParserTests_If
    {
        private static Parser ClassUnderTest(IList<IToken> tokens) => new Parser(tokens);

        [Fact]
        public void Given_valid_if_expression_without_else_then_returns_if_without_else()
        {
            // Arrange
            var condToken = Dat(TokenType.True, "true", true);
            var ifToken = Dat(TokenType.String, "\"hello\"", "hello");
            var tokens = Ls(
                Emp(TokenType.If, "if"),
                condToken,
                Emp(TokenType.Then, "then"),
                ifToken,
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<IfExpr>();
            var ifExpr = result.AsIf();
            ifExpr.Condition.Should().Be(CreateLiteral(condToken, Primatives.BoolType, true));
            ifExpr.IfTrue.Should().Be(CreateLiteral(ifToken, Primatives.StringType, "hello"));
            ifExpr.IfFalse.Should().BeNull();
        }

        [Fact]
        public void Given_valid_if_expression_with_else_then_returns_if_with_else()
        {
            // Arrange
            var condToken = Dat(TokenType.True, "true", true);
            var ifToken = Dat(TokenType.String, "\"hello\"", "hello");
            var elseToken = Dat(TokenType.Number, "123", 123);
            var tokens = Ls(
                Emp(TokenType.If, "if"),
                condToken,
                Emp(TokenType.Then, "then"),
                ifToken,
                Emp(TokenType.Else, "else"),
                elseToken,
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<IfExpr>();
            var ifExpr = result.AsIf();
            ifExpr.Condition.Should().Be(CreateLiteral(condToken, Primatives.BoolType, true));
            ifExpr.IfTrue.Should().Be(CreateLiteral(ifToken, Primatives.StringType, "hello"));
            ifExpr.IfFalse.Should().Be(CreateLiteral(elseToken, Primatives.NumType, 123));
        }

        [Fact]
        public void Given_if_expression_without_condition_then_errors()
        {
            // Arrange
            var ifToken = Dat(TokenType.String, "\"hello\"", "hello");
            var thenToken = Emp(TokenType.Then, "then");
            var tokens = Ls(
                Emp(TokenType.If, "if"),
                thenToken,
                ifToken,
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.Error.Should().NotBeNull();
            result.Error.Message.Should().Be(Messages.ExpectedExpression(thenToken));
        }

        [Fact]
        public void Given_if_expression_with_if_return_condition_then_returns_return_expr()
        {
            // Arrange
            var condToken = Dat(TokenType.True, "true", true);
            var returnToken = Emp(TokenType.Return, "return");
            var exprToken = Dat(TokenType.String, "\"hello\"", "hello");
            var tokens = Ls(
                Emp(TokenType.If, "if"),
                condToken,
                Emp(TokenType.Then, "then"),
                returnToken,
                exprToken,
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<IfExpr>();
            var ifExpr = result.AsIf();
            ifExpr.Condition.Should().Be(CreateLiteral(condToken, Primatives.BoolType, true));
            ifExpr.IfTrue.Should().Be(new ReturnExpr(returnToken, CreateLiteral(exprToken, Primatives.StringType, "hello")));
            ifExpr.IfFalse.Should().BeNull();
        }

        [Fact]
        public void Given_if_expression_with_else_return_condition_then_returns_return_expr()
        {
            // Arrange
            var condToken = Dat(TokenType.True, "true", true);
            var ifToken = Dat(TokenType.String, "\"hello\"", "hello");
            var returnToken = Emp(TokenType.Return, "return");
            var exprToken = Dat(TokenType.String, "\"hello\"", "hello");
            var tokens = Ls(
                Emp(TokenType.If, "if"),
                condToken,
                Emp(TokenType.Then, "then"),
                ifToken,
                Emp(TokenType.Else, "else"),
                returnToken,
                exprToken,
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<IfExpr>();
            var ifExpr = result.AsIf();
            ifExpr.Condition.Should().Be(CreateLiteral(condToken, Primatives.BoolType, true));
            ifExpr.IfTrue.Should().Be(CreateLiteral(exprToken, Primatives.StringType, "hello"));
            ifExpr.IfFalse.Should().Be(new ReturnExpr(returnToken, CreateLiteral(exprToken, Primatives.StringType, "hello")));
        }

        [Fact]
        public void Given_if_expression_with_branches_returning_then_returns_return_expr()
        {
            // Arrange
            var condToken = Dat(TokenType.True, "true", true);
            var ifToken = Dat(TokenType.Number, "1", 1);
            var returnToken = Emp(TokenType.Return, "return");
            var elseToken = Dat(TokenType.String, "\"hello\"", "hello");
            var tokens = Ls(
                Emp(TokenType.If, "if"),
                condToken,
                Emp(TokenType.Then, "then"),
                returnToken,
                ifToken,
                Emp(TokenType.Else, "else"),
                returnToken,
                elseToken,
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<IfExpr>();
            var ifExpr = result.AsIf();
            ifExpr.Condition.Should().Be(CreateLiteral(condToken, Primatives.BoolType, true));
            ifExpr.IfTrue.Should().Be(new ReturnExpr(returnToken, CreateLiteral(ifToken, Primatives.NumType, 1)));
            ifExpr.IfFalse.Should().Be(new ReturnExpr(returnToken, CreateLiteral(elseToken, Primatives.StringType, "hello")));
        }
    }
}