using System.Collections.Generic;
using Ether.Core.Expressions;
using Ether.Core.Parsing;
using Ether.Core.Statements;
using Ether.Core.Tokens;
using Ether.Core.Types;
using FluentAssertions;
using Xunit;
using static Ether.Tests.Core.ExprTestHelpers;
using static Ether.Tests.Core.TokenTestHelpers;

namespace Ether.Tests.Core.Parsing
{
    public class ParserTests_Block
    {
        private static Parser ClassUnderTest(IList<IToken> tokens) => new Parser(tokens);

        [Fact]
        public void Given_function_definition_with_block_then_returns_expression()
        {
            // Arrange
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Emp(TokenType.LeftBrace, "{"),
                Dat(TokenType.Number, "1", 1),
                Emp(TokenType.RightBrace, "}"),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<Function>();
            var func = result.AsFunc();
            func.Body.Should().BeOfType<Block>();
        }

        [Fact]
        public void Given_function_definition_with_block_then_parses_statements_correctly()
        {
            // Arrange
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Emp(TokenType.LeftBrace, "{"),
                Emp(TokenType.Let, "let"),
                Dat(TokenType.Identifier, "a", "a"),
                Emp(TokenType.Equal, "="),
                Dat(TokenType.String, "Bob", "Bob"),
                Semi(),
                Dat(TokenType.Number, "1", 1),
                Emp(TokenType.RightBrace, "}"),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<Function>();
            var block = result.GetFuncBody();
            block.Statements.Should().HaveCount(2);
            block.Statements[0].Should().BeOfType<LetStmt>();
            block.Statements[1].Should().BeOfType<ExprReturnStmt>();
        }

        [Fact]
        public void Given_function_block_with_no_return_expression_then_parses_statements_correctly()
        {
            // Arrange
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Emp(TokenType.LeftBrace, "{"),
                Emp(TokenType.Let, "let"),
                Dat(TokenType.Identifier, "a", "a"),
                Emp(TokenType.Equal, "="),
                Dat(TokenType.String, "Bob", "Bob"),
                Semi(),
                Emp(TokenType.RightBrace, "}"),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<Function>();
            var block = result.GetFuncBody();
            block.Statements.Should().HaveCount(1);
            block.Statements[0].Should().BeOfType<LetStmt>();
        }

        [Fact]
        public void Given_function_block_with_expression_before_end_then_errors()
        {
            // Arrange
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Emp(TokenType.LeftBrace, "{"),
                Dat(TokenType.Number, "1", 1),
                Emp(TokenType.Let, "let"),
                Dat(TokenType.Identifier, "a", "a"),
                Emp(TokenType.Equal, "="),
                Dat(TokenType.String, "Bob", "Bob"),
                Semi(),
                Emp(TokenType.RightBrace, "}"),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.Error.Should().NotBeNull();
            result.Error.Message.Should().Be(Messages.ExpressionCanOnlyBeLastBlockItem);
        }

        [Fact]
        public void Given_function_definition_with_empty_block_then_errors()
        {
            // Arrange
            var ifToken = Dat(TokenType.String, "\"hello\"", "hello");
            var closingBrace = Emp(TokenType.RightBrace, "}");
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Emp(TokenType.LeftBrace, "{"),
                closingBrace,
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.Error.Should().NotBeNull();
            result.Error.Message.Should().Be(Messages.ExpectedExpression(closingBrace));
        }

        [Fact]
        public void Given_return_statement_then_parses_correctly()
        {
            // Arrange
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Emp(TokenType.LeftBrace, "{"),
                Emp(TokenType.Return, "return"),
                Dat(TokenType.Number, "1", 1),
                Semi(),
                Emp(TokenType.RightBrace, "}"),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.AsExpr().Should().BeOfType<Function>();
            var block = result.GetFuncBody();
            block.Statements.Should().HaveCount(1);
            block.Statements[0].Should().BeOfType<ReturnStmt>();
        }

        [Fact]
        public void Given_other_statements_after_return_then_returns_errors()
        {
            // Arrange
            var tokens = Ls(
                Emp(TokenType.LeftParen, "("),
                Emp(TokenType.RightParen, ")"),
                Emp(TokenType.Arrow, "=>"),
                Emp(TokenType.LeftBrace, "{"),
                Emp(TokenType.Return, "return"),
                Dat(TokenType.Number, "1", 1),
                Semi(),
                Emp(TokenType.Let, "let"),
                Dat(TokenType.Identifier, "a", "a"),
                Emp(TokenType.Equal, "="),
                Dat(TokenType.String, "Bob", "Bob"),
                Semi(),
                Emp(TokenType.RightBrace, "}"),
                Semi()
            );

            // Act
            var result = ClassUnderTest(tokens).ParseProgram();

            // Assert
            result.Error.Should().NotBeNull();
            result.Error.Message.Should().Be(Messages.UnreachableCodeAfterReturn);
        }
    }
}