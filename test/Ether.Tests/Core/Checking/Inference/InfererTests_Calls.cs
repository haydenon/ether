using System;
using System.Collections.Generic;
using System.Linq;
using Ether.Core.Checking.Inference;
using Ether.Core.Expressions;
using Ether.Core.Tokens;
using Ether.Core.Types;
using FluentAssertions;
using Xunit;
using static Ether.Tests.Core.ExprTestHelpers;
using static Ether.Tests.Core.Checking.Inference.InferenceTestsCommon;
using static Ether.Core.Checking.Inference.Messages;

namespace Ether.Tests.Core.Checking.Inference
{
    public class InfererTests_Calls
    {
        private static readonly IToken AnyToken = new EmptyToken(TokenType.LeftParen, "(", 1);

        #region Arguments

        [Fact]
        public void Given_more_arguments_than_function_then_throws_error()
        {
            // Arrange
            var literal = CreateLiteral("Num");
            var function = new Function(Params(("x", "Num")), literal, literal.Type);
            var call = new CallExpr(function, AnyToken, new[] { CreateLiteral("Num"), CreateLiteral("String") });

            // Act
            Action action = () => Inferer.Infer(call, null, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TooManyArguments(function.Parameters.Count, call.Arguments.Count));
        }

        [Fact]
        public void Given_argument_of_wrong_type_then_throws_error()
        {
            // Arrange
            var literal = CreateLiteral("Num");
            var function = new Function(Params(("x", "Num")), literal, literal.Type);
            var call = new CallExpr(function, AnyToken, new[] { CreateLiteral("String") });

            // Act
            Action action = () => Inferer.Infer(call, null, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchError(Plain("Num"), Plain("String")));
        }

        [Fact]
        public void Given_less_arguments_than_function_then_returns_partially_applied_function()
        {
            // Arrange
            var literal = CreateLiteral("Num");
            var function = new Function(Params(("x", "Num"), ("y", "String"), ("z", "Bool")), literal, literal.Type);
            var call = new CallExpr(function, AnyToken, new[] { CreateLiteral("Num") });

            // Act
            var type = Inferer.Infer(call, null, Context.Empty);

            // Assert
            type.Should().BeOfType<TFunc>();
            type.As<TFunc>().Generics.Should().Equal(new IType[] { new TPlain("String"), new TPlain("Bool"), literal.Type });
            // var generics = type.As<TFunc>().Generics;
            // generics.Should().HaveCount(2);
            // generics[0].Should().Be(new TPlain("Num"));
            // generics[1].As<TFunc>()
        }

        private static readonly IList<FunctionParameter> ApplicationParams =
            Params(
                ("a", "String"),
                ("b", "Num"),
                ("c", "Bool"),
                ("d", "Date"),
                ("e", "Guid"),
                ("f", "Other")
            );

        public static IEnumerable<object[]> GetDifferentApplications()
        {
            yield return CreateApplicationData(ApplicationParams, 1);
            yield return CreateApplicationData(ApplicationParams, 2);
            yield return CreateApplicationData(ApplicationParams, 3);
            yield return CreateApplicationData(ApplicationParams, 4);
            yield return CreateApplicationData(ApplicationParams, 5);
        }

        [Theory]
        [MemberData(nameof(GetDifferentApplications))]
        public void Given_different_applied_arguments_then_return_type_with_remaining(
            IList<FunctionParameter> parameters,
            IList<IExpr> arguments,
            IList<IType> remaining)
        {
            // Arrange
            var literal = CreateLiteral("Num");
            var function = new Function(parameters, literal, literal.Type);
            var call = new CallExpr(function, AnyToken, arguments);

            // Act
            var type = Inferer.Infer(call, null, Context.Empty);

            // Assert
            type.Should().BeOfType<TFunc>();
            type.As<TFunc>().Generics.Should().Equal(remaining.Concat(new[] { literal.Type }));
        }

        #endregion

        [Fact]
        public void Given_function_call_as_result_of_other_call_then_correctly_determines_type()
        {
            // Arrange
            var innerFunctionType = new TFunc(new[] { Plain("Num"), Plain("String"), Plain("Bool") });
            var function = new Function(Params(("$", "Unit")), CreateVariable("a"), null);
            var call = new CallExpr(function, AnyToken, new[] { CreateLiteral("Unit") });
            var context = Context.Empty.Add(innerFunctionType, "a");

            // Act
            var type = Inferer.Infer(call, null, context);

            // Assert
            type.Should().BeOfType<TFunc>();
            type.As<TFunc>().Generics.Should().Equal(new IType[] { Plain("Num"), Plain("String"), Plain("Bool") });
        }

        private static object[] CreateApplicationData(IList<FunctionParameter> parameters, int applied)
            => new object[]{
                parameters,
                parameters.Take(applied).Select(fp => CreateLiteral(fp.TypeAnnotation!.Name)).ToArray(),
                parameters.Skip(applied).Select(fp => fp.TypeAnnotation!).ToArray()
            };
    }
}