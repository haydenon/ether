using System;
using System.Collections.Generic;
using Ether.Core.Checking.Inference;
using Ether.Core.Expressions;
using Ether.Core.Types;
using FluentAssertions;
using Xunit;
using static Ether.Core.Checking.Inference.Messages;
using static Ether.Tests.Core.ExprTestHelpers;

namespace Ether.Tests.Core.Checking.Inference
{
    public class InfererTests
    {
        public static IEnumerable<object[]> GetLiteralTypes()
        {
            yield return CreateLiteralData("Num");
            yield return CreateLiteralData("Bool");
            yield return CreateLiteralData("String");
            yield return CreateLiteralData("RandomType");
        }

        [Theory]
        [MemberData(nameof(GetLiteralTypes))]
        public void Given_literal_then_infers_type(IExpr literal, string expectedType)
        {
            // Act
            var type = Inferer.Infer(literal, null, Context.Empty);

            // Assert
            type.Should().BeEquivalentTo(new TPlain(expectedType));
        }

        [Fact]
        public void Given_literal_that_doesnt_match_type_then_throws_error()
        {
            // Arrange
            var literal = CreateLiteral("Num");
            var expectedType = new TPlain("NotNum");

            // Act
            Action action = () => Inferer.Infer(literal, expectedType, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchError(expectedType, literal.Type));
        }

        [Fact]
        public void Given_bound_variable_then_return_context_type()
        {
            // Arrange
            var varName = "anyvar";
            var variable = CreateVariable(varName);
            var varType = new TPlain("Num");
            var context = Context.Empty.Add(varType, varName);

            // Act
            var type = Inferer.Infer(variable, null, context);

            // Assert
            type.Should().BeEquivalentTo(varType);
        }

        [Fact]
        public void Given_unbound_variable_then_throws_error()
        {
            // Arrange
            var variable = CreateVariable("anyvar");

            // Act
            Action action = () => Inferer.Infer(variable, null, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(UnboundVariableError(variable));
        }

        [Fact]
        public void Given_variable_that_doesnt_match_type_then_throws_error()
        {
            // Arrange
            var varName = "anyvar";
            var variable = CreateVariable(varName);
            var varType = new TPlain("Num");
            var context = Context.Empty.Add(varType, varName);
            var expectedType = new TPlain("NotNum");

            // Act
            Action action = () => Inferer.Infer(variable, expectedType, context);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchError(expectedType, varType));
        }

        private static object[] CreateLiteralData(string type)
                    => new object[] { CreateLiteral(type), type };
    }
}