using System;
using Ether.Core.Checking.Inference;
using Ether.Core.Expressions;
using Ether.Core.Types;
using FluentAssertions;
using Xunit;
using static Ether.Tests.Core.ExprTestHelpers;
using static Ether.Tests.Core.StmtTestHelpers;
using static Ether.Tests.Core.Checking.Inference.InferenceTestsCommon;
using static Ether.Core.Checking.Inference.Messages;

namespace Ether.Tests.Core.Checking.Inference
{
    public class InfererTests_FunctionBlocks
    {
        [Fact]
        public void Given_function_with_type_then_infers_type()
        {
            // Arrange
            var block = CreateBlock(
                CreateExprReturn(CreateLiteral("Num"))
            );
            var function = new Function(Params("x"), block, null);

            // Act
            var type = Inferer.Infer(function, null, Context.Empty);

            // Assert
            type.Should().NotBeNull();
            type.Should().BeOfType<TFunc>();
        }

        [Fact]
        public void Given_function_block_with_expected_type_then_infers_type()
        {
            // Arrange
            var block = CreateBlock(
                CreateExprReturn(CreateLiteral("Num"))
            );
            var function = new Function(Params("x"), block, null);
            var expectedType = new TFunc(new[] { new TPlain("Num"), new TPlain("Num") });

            // Act
            var type = Inferer.Infer(function, expectedType, Context.Empty);

            // Assert
            type.Should().NotBeNull();
            type.Should().BeOfType<TFunc>();
        }

        [Fact]
        public void Given_function_without_expr_return_then_is_unit()
        {
            // Arrange
            var block = CreateBlock(CreateLet("y", CreateLiteral("Num")));
            var function = new Function(Params(("x", "String")), block, null);
            var expectedType = new TFunc(new[] { new TPlain("String"), Primatives.UnitType });

            // Act
            var type = Inferer.Infer(function, null, Context.Empty);

            // Assert
            type.Should().BeOfType<TFunc>();
            type.As<TFunc>().Generics.Should().Equal(expectedType.generics);
        }

        [Fact]
        public void Given_block_doesnt_match_function_expected_return_then_throw_error()
        {
            // Arrange
            var block = CreateBlock(CreateExprReturn(CreateLiteral("Num")));
            var function = new Function(Params(("x")), block, new TPlain("String"));

            // Act
            Action action = () => Inferer.Infer(function, null, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchError(new TPlain("String"), new TPlain("Num")));
        }

        [Fact]
        public void Given_function_with_block_doesnt_match_expected_type_then_throw_error()
        {
            // Arrange
            var block = CreateBlock(CreateExprReturn(CreateLiteral("Num")));
            var function = new Function(Params(("x")), block, null);
            var expectedType = new TFunc(new[] { new TPlain("Num"), new TPlain("String") });

            // Act
            Action action = () => Inferer.Infer(function, expectedType, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchError(new TPlain("String"), new TPlain("Num")));
        }
    }
}