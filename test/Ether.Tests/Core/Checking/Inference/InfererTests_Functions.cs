using System;
using Ether.Core.Checking.Inference;
using Ether.Core.Expressions;
using Ether.Core.Types;
using FluentAssertions;
using Xunit;
using static Ether.Tests.Core.ExprTestHelpers;
using static Ether.Tests.Core.Checking.Inference.InferenceTestsCommon;
using static Ether.Core.Checking.Inference.Messages;

namespace Ether.Tests.Core.Checking.Inference
{
    public class InfererTests_Functions
    {
        [Fact]
        public void Given_function_then_infers_type()
        {
            // Arrange
            var literal = CreateLiteral("Num");
            var function = new Function(Params("x"), literal, null);
            var expectedType = new TFunc(new[] { new TPlain("String"), literal.Type });

            // Act
            var type = Inferer.Infer(function, null, Context.Empty);

            // Assert
            type.Should().NotBeNull();
            type.Should().BeOfType<TFunc>();
        }

        [Fact]
        public void Given_function_with_param_type_then_infers_type()
        {
            // Arrange
            var literal = CreateLiteral("Num");
            var function = new Function(Params(("x", "String")), literal, null);
            var expectedType = new TFunc(new[] { new TPlain("String"), literal.Type });

            // Act
            var type = Inferer.Infer(function, null, Context.Empty);

            // Assert
            type.Should().BeOfType<TFunc>();
            type.As<TFunc>().Generics.Should().Equal(expectedType.generics);
        }

        [Fact]
        public void Given_expected_function_has_differing_parameters_then_throw_error()
        {
            // Arrange
            var literal = CreateLiteral("Num");
            var p1 = new TPlain("Num");
            var p2 = new TPlain("Bool");
            var p1Expected = new TPlain("NotNum");
            var actualType = new TFunc(new[] { p1, p2, literal.Type });
            var function = new Function(Params(("x", p1.Name), ("y", p2.Name)), literal, null);
            var expectedType = new TFunc(new[] { p1Expected, new TPlain("NotBool"), literal.Type });

            // Act
            Action action = () => Inferer.Infer(function, expectedType, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchError(p1Expected, p1));
        }

        [Fact]
        public void Given_different_number_of_parameters_then_throw_error()
        {
            // Arrange
            var literal = CreateLiteral("Num");
            var p1 = new TPlain("Num");
            var p2 = new TPlain("Bool");
            var function = new Function(Params(("x", p1.Name), ("y", p2.Name)), literal, null);
            var expectedType = new TFunc(new[] { p1, literal.Type });

            // Act
            Action action = () => Inferer.Infer(function, expectedType, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchWithExpected(p1));
        }

        [Fact]
        public void Given_different_return_type_then_throws_error()
        {
            // Arrange
            var literal = CreateLiteral("Num");
            var p1 = new TPlain("Num");
            var function = new Function(Params(("x", p1.Name)), literal, null);
            var expectedRetType = new TPlain("NotNum");
            var expectedType = new TFunc(new[] { p1, expectedRetType });

            // Act
            Action action = () => Inferer.Infer(function, expectedType, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchWithExpected(expectedRetType));
        }
    }
}