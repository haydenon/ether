using System;
using System.Collections.Generic;
using System.Linq;
using Ether.Core.Checking.Inference;
using Ether.Core.Expressions;
using Ether.Core.Tokens;
using Ether.Core.Types;
using FluentAssertions;
using Xunit;
using static Ether.Tests.Core.ExprTestHelpers;
using static Ether.Core.Checking.Inference.Messages;

namespace Ether.Tests.Core.Checking.Inference
{
    public class InfererTests_Arrays
    {
        private static readonly IToken OpToken = new EmptyToken(TokenType.LeftSquare, "[", 1);

        public static IEnumerable<object[]> GetNestedArrays()
        {
            yield return CreateNestedArrayData(1, "Num", "Array<Num>");
            yield return CreateNestedArrayData(2, "String", "Array<Array<String>>");
            yield return CreateNestedArrayData(3, "Num", "Array<Array<Array<Num>>>");
            yield return CreateNestedArrayData(4, "AnyType", "Array<Array<Array<Array<AnyType>>>>");
        }

        [Theory]
        [MemberData(nameof(GetNestedArrays))]
        public void Given_literal_then_infers_type(IExpr nestedArray, string expectedType)
        {
            // Act
            var type = Inferer.Infer(nestedArray, null, Context.Empty);

            // Assert
            type.Should().BeOfType<TConstructor>();
            type.ToString().Should().Be(expectedType);
        }

        [Fact]
        public void Given_value_doesnt_match_annotation_then_throws_error()
        {
            // Arrange
            var expectedType = new TPlain("NotNum");
            var arrayValue = CreateLiteral("Num");
            var array = new ArrayConstructor(OpToken, new[] { arrayValue }, expectedType);

            // Act
            Action action = () => Inferer.Infer(array, null, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchError(expectedType, arrayValue.Type));
        }

        [Fact]
        public void Given_value_doesnt_match_expected_type_then_throws_error()
        {
            // Arrange
            var innerType = new TPlain("NotNum");
            var expectedType = new TConstructor("Array", new[] { innerType });
            var arrayValue = CreateLiteral("Num");
            var array = new ArrayConstructor(OpToken, new[] { arrayValue }, null);

            // Act
            Action action = () => Inferer.Infer(array, expectedType, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchError(innerType, arrayValue.Type));
        }

        [Fact]
        public void Given_values_of_differing_types_then_throws_error()
        {
            // Arrange
            var val1 = CreateLiteral("Num");
            var val2 = CreateLiteral("Num");
            var val3 = CreateLiteral("String");
            var array = new ArrayConstructor(OpToken, new[] { val1, val2, val3 }, null);

            // Act
            Action action = () => Inferer.Infer(array, null, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchError(val1.Type, val3.Type));
        }

        private static object[] CreateNestedArrayData(int levels, string type, string expectedType)
        {
            var baseExpr = CreateLiteral(type);
            var arrayExpr = Enumerable.Range(0, levels).Aggregate(baseExpr as IExpr, (acc, _) =>
                new ArrayConstructor(OpToken, new[] { acc }, null)
            );
            return new object[] { arrayExpr, expectedType };
        }
    }
}