using System;
using Ether.Core.Expressions;
using Ether.Core.Checking.Inference;
using Ether.Core.Types;
using FluentAssertions;
using Xunit;
using static Ether.Tests.Core.ExprTestHelpers;
using static Ether.Tests.Core.Checking.Inference.InferenceTestsCommon;
using System.Collections.Generic;
using Ether.Core.Statements;

namespace Ether.Tests.Core.Checking.Inference
{
    public class InfererTests_Return
    {
        [Fact]
        public void Given_if_with_one_return_then_uses_other_type()
        {
            // Arrange
            var ifExpr = new IfExpr(CreateLiteral("Bool"), CreateLiteral("Num"), CreateReturnExpr(CreateLiteral("String")));
            var expectedRetType = new TPlain("String");
            var func = CreateFunc(ifExpr);

            // Act
            Action action = () => Inferer.Infer(func, null, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchWithExpected(expectedRetType));
        }

        [Fact]
        public void Given_if_with_one_return_and_else_then_is_valid()
        {
            // Arrange
            var ifExpr = new IfExpr(CreateLiteral("Bool"), CreateReturnExpr(CreateLiteral("String")), CreateLiteral("Num"));
            var blockExpr = new Block(new List<IStmt> { new ExprStmt(ifExpr), new ExprReturnStmt(CreateLiteral("String")) });
            var func = CreateFunc(blockExpr);

            // Act
            var type = Inferer.Infer(func, null, Context.Empty);

            // Assert
            type.Should().NotBeNull();
            type.Should().BeOfType<TFunc>();
        }

        [Fact]
        public void Given_if_with_two_returns_for_func_expr_then_is_valid()
        {
            // Arrange
            var ifExpr = new IfExpr(CreateLiteral("Bool"), CreateReturnExpr(CreateLiteral("String")), CreateReturnExpr(CreateLiteral("String")));
            var func = CreateFunc(ifExpr);

            // Act
            var type = Inferer.Infer(func, null, Context.Empty);

            // Assert
            type.Should().NotBeNull();
            type.Should().BeOfType<TFunc>();
            type.As<TFunc>().Generics[1].Should().BeEquivalentTo(new TPlain("String"));
        }

        [Fact]
        public void Given_if_with_two_returns_for_func_with_block_then_is_valid()
        {
            // Arrange
            var ifExpr = new IfExpr(CreateLiteral("Bool"), CreateReturnExpr(CreateLiteral("String")), CreateReturnExpr(CreateLiteral("String")));
            var blockExpr = new Block(new List<IStmt> { new ExprStmt(ifExpr) });
            var func = CreateFunc(blockExpr);

            // Act
            var type = Inferer.Infer(func, null, Context.Empty);

            // Assert
            type.Should().NotBeNull();
            type.Should().BeOfType<TFunc>();
            type.As<TFunc>().Generics[1].Should().BeEquivalentTo(new TPlain("String"));
        }
    }
}