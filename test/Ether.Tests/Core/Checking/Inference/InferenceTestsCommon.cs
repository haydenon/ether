using System.Collections.Generic;
using System.Linq;
using Ether.Core.Checking.Inference;
using Ether.Core.Expressions;
using Ether.Core.Tokens;
using Ether.Core.Types;

namespace Ether.Tests.Core.Checking.Inference
{
    public static class InferenceTestsCommon
    {
        public static string TypeMismatchWithExpected(IType expected)
            => string.Join("\n", Messages.TypeMismatchError(expected, new TPlain("")).Split("\n").Take(2)) + "*";

        public static IList<FunctionParameter> Params(params string[] names)
            => names.Select(n => new FunctionParameter(new EmptyToken(TokenType.Identifier, n, 1), null)).ToList();

        public static IList<FunctionParameter> Params(params (string name, string type)[] fparams)
            => fparams.Select(fp => new FunctionParameter(new EmptyToken(TokenType.Identifier, fp.name, 1), new TPlain(fp.type))).ToList();

        public static TPlain Plain(string name) => new TPlain(name);
    }
}