using System;
using Ether.Core.Checking.Inference;
using Ether.Core.Expressions;
using Ether.Core.Types;
using FluentAssertions;
using Xunit;
using static Ether.Tests.Core.ExprTestHelpers;
using static Ether.Tests.Core.Checking.Inference.InferenceTestsCommon;

namespace Ether.Tests.Core.Checking.Inference
{
    public class InfererTests_If
    {
        [Fact]
        public void Given_if_then_infers_type()
        {
            // Arrange
            var ifExpr = new IfExpr(CreateLiteral("Bool"), CreateLiteral("Num"), CreateLiteral("Num"));

            // Act
            var type = Inferer.Infer(ifExpr, null, Context.Empty);

            // Assert
            type.Should().NotBeNull();
            type.Should().BeEquivalentTo(new TPlain("Num"));
        }

        [Fact]
        public void Given_if_with_no_else_then_infers_unit()
        {
            // Arrange
            var ifExpr = new IfExpr(CreateLiteral("Bool"), CreateLiteral("Unit"), null);

            // Act
            var type = Inferer.Infer(ifExpr, null, Context.Empty);

            // Assert
            type.Should().NotBeNull();
            type.Should().BeEquivalentTo(new TPlain("Unit"));
        }

        [Fact]
        public void Given_if_with_different_branch_types_then_throws_error()
        {
            // Arrange
            var ifExpr = new IfExpr(CreateLiteral("Bool"), CreateLiteral("Num"), CreateLiteral("String"));
            var expectedRetType = new TPlain("Num");

            // Act
            Action action = () => Inferer.Infer(ifExpr, null, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchWithExpected(expectedRetType));
        }


        [Fact]
        public void Given_if_with_return_out_of_function_then_throws_error()
        {
            // Arrange
            var ifExpr = new IfExpr(CreateLiteral("Bool"), CreateReturnExpr(CreateLiteral("String")), CreateLiteral("Num"));
            var expectedRetType = new TPlain("Unit");

            // Act
            Action action = () => Inferer.Infer(ifExpr, null, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(Messages.ReturnOutOfFunction);
        }

        [Fact]
        public void Given_if_with_non_unit_and_no_else_then_throws_error()
        {
            // Arrange
            var ifExpr = new IfExpr(CreateLiteral("Bool"), CreateLiteral("Num"), null);
            var expectedRetType = new TPlain("Unit");

            // Act
            Action action = () => Inferer.Infer(ifExpr, null, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchWithExpected(expectedRetType));
        }

        [Fact]
        public void Given_if_with_non_unit_and_matching_expected_type_and_no_else_then_throws_error()
        {
            // Arrange
            var ifExpr = new IfExpr(CreateLiteral("Bool"), CreateLiteral("Num"), null);
            var expectedType = new TPlain("Num");
            var expectedRetType = new TPlain("Unit");

            // Act
            Action action = () => Inferer.Infer(ifExpr, expectedType, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchWithExpected(expectedRetType));
        }

        [Fact]
        public void Given_if_with_matching_branches_and_mismatched_expected_type_then_throws_error()
        {
            // Arrange
            var ifExpr = new IfExpr(CreateLiteral("Bool"), CreateLiteral("Num"), CreateLiteral("Num"));
            var expectedType = new TPlain("String");
            var expectedRetType = new TPlain("String");

            // Act
            Action action = () => Inferer.Infer(ifExpr, expectedType, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchWithExpected(expectedRetType));
        }

        [Fact]
        public void Given_if_with_non_bool_condition_then_throws_error()
        {
            // Arrange
            var ifExpr = new IfExpr(CreateLiteral("String"), CreateLiteral("Num"), CreateLiteral("Num"));
            var expectedRetType = new TPlain("Bool");

            // Act
            Action action = () => Inferer.Infer(ifExpr, null, Context.Empty);

            // Assert
            action.Should().Throw<InferenceException>().WithMessage(TypeMismatchWithExpected(expectedRetType));
        }
    }
}