using System.Collections.Generic;
using System.Linq;
using Ether.Core.Tokens;

namespace Ether.Tests.Core
{
    public static class TokenTestHelpers
    {
        public static List<IToken> Ls(params IToken[] tokens)
            => new List<IToken>(tokens).Concat(!tokens.Any() || tokens.Last().TokenType != TokenType.Eof ? new[]{
                Emp(TokenType.Eof, "", tokens.LastOrDefault()?.Line ?? 1)
            } : new IToken[0]).ToList();

        public static IToken Emp(TokenType type, string lexeme, int line = 1)
            => new EmptyToken(type, lexeme, line);

        public static IToken Dat(TokenType type, string lexeme, object data, int line = 1)
            => new DataToken(type, lexeme, line, data);

        public static IToken Semi(int line = 1)
            => new EmptyToken(TokenType.SemiColon, ";", line);
    }
}