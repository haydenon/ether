using Ether.Core.Expressions;
using Ether.Core.Statements;
using Ether.Core.Tokens;
using Ether.Core.Types;

namespace Ether.Tests.Core
{
    public static class StmtTestHelpers
    {
        public static ExprReturnStmt CreateExprReturn(IExpr expr) => new ExprReturnStmt(expr);

        public static LetStmt CreateLet(string name, IExpr expr, IType expectedType = null)
            => new LetStmt(new EmptyToken(TokenType.Identifier, name, 1), expr, expectedType);
    }
}